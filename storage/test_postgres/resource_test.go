package test

import (
	"context"
	"testing"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"github.com/smart-city/emergency-response-service/storage/postgres"
	"github.com/stretchr/testify/assert"
)

func TestCreateResource(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)
	defer db.Close(context.Background())

	resourceRepo := postgres.NewResource(db)

	// Test data
	testResource := &emergency.CreateResourceRequest{
		ResourceType: "Ambulance",
		Latitude:     "34.0522",
		Longitude:    "-118.2437",
		Status:       "available",
	}

	// Call the CreateResource method
	response, err := resourceRepo.CreateResource(context.Background(), testResource)

	// Assertions
	assert.NoError(t, err, "CreateResource should not return an error")
	assert.NotNil(t, response, "CreateResource response should not be nil")
	assert.NotEmpty(t, response.ResourceId, "Created resource should have a valid ID")

	// Clean up the created resource
	_, err = db.Exec(context.Background(), "DELETE FROM emergency_resources WHERE resource_id = $1", response.ResourceId)
	assert.NoError(t, err, "Cleanup: failed to delete test resource")
}

func TestGetResource(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)
	defer db.Close(context.Background())

	resourceRepo := postgres.NewResource(db)

	// 1. First, create a resource to retrieve
	testResource := &emergency.CreateResourceRequest{
		ResourceType: "Fire Truck",
		Latitude:     "37.7749",
		Longitude:    "-122.4194",
		Status:       "dispatched",
	}
	createResponse, err := resourceRepo.CreateResource(context.Background(), testResource)
	assert.NoError(t, err, "Creating resource for GetResource test failed")
	assert.NotEmpty(t, createResponse.ResourceId, "Created resource should have a valid ID")

	// 2. Now, try to retrieve the created resource
	getResourceRequest := &emergency.GetResourceRequest{ResourceId: createResponse.ResourceId}
	getResponse, err := resourceRepo.GetResource(context.Background(), getResourceRequest)

	// Assertions for GetResource
	assert.NoError(t, err, "GetResource should not return an error")
	assert.NotNil(t, getResponse, "GetResource response should not be nil")
	assert.Equal(t, testResource.ResourceType, getResponse.Resource.ResourceType, "Resource type should match")
	assert.Equal(t, testResource.Status, getResponse.Resource.Status, "Resource status should match")
	// Add assertions for latitude and longitude if needed, converting to float and comparing with some tolerance.

	// Clean up
	_, err = db.Exec(context.Background(), "DELETE FROM emergency_resources WHERE resource_id = $1", createResponse.ResourceId)
	assert.NoError(t, err, "Cleanup: failed to delete test resource")
}

func TestUpdateResource(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)
	defer db.Close(context.Background())

	resourceRepo := postgres.NewResource(db)

	// 1. Create a resource to update
	testResource := &emergency.CreateResourceRequest{
		ResourceType: "Police Car",
		Latitude:     "40.7128",
		Longitude:    "-74.0060",
		Status:       "available",
	}
	createResponse, err := resourceRepo.CreateResource(context.Background(), testResource)
	assert.NoError(t, err, "Creating resource for UpdateResource test failed")
	assert.NotEmpty(t, createResponse.ResourceId, "Created resource should have a valid ID")

	// 2. Update the created resource
	updateResourceRequest := &emergency.UpdateResourceRequest{
		ResourceId:   createResponse.ResourceId,
		ResourceType: "Ambulance",
		Latitude:     "41.8781",
		Longitude:    "-87.6298",
		Status:       "dispatched",
	}
	updateResponse, err := resourceRepo.UpdateResource(context.Background(), updateResourceRequest)

	// Assertions for UpdateResource
	assert.NoError(t, err, "UpdateResource should not return an error")
	assert.NotNil(t, updateResponse, "UpdateResource response should not be nil")
	assert.Equal(t, "success", updateResponse.Status, "UpdateResource should return success status")

	// 3. Retrieve the updated resource to verify changes
	getResourceRequest := &emergency.GetResourceRequest{ResourceId: createResponse.ResourceId}
	getResponse, err := resourceRepo.GetResource(context.Background(), getResourceRequest)
	assert.NoError(t, err, "GetResource after update should not return an error")
	assert.Equal(t, updateResourceRequest.ResourceType, getResponse.Resource.ResourceType, "Resource type should be updated")
	assert.Equal(t, updateResourceRequest.Status, getResponse.Resource.Status, "Resource status should be updated")
	// Add assertions for latitude and longitude, similar to TestGetResource

	// Clean up
	_, err = db.Exec(context.Background(), "DELETE FROM emergency_resources WHERE resource_id = $1", createResponse.ResourceId)
	assert.NoError(t, err, "Cleanup: failed to delete test resource")
}

func TestDeleteResource(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)
	defer db.Close(context.Background())

	resourceRepo := postgres.NewResource(db)

	// 1. Create a resource to delete
	testResource := &emergency.CreateResourceRequest{
		ResourceType: "Fire Truck",
		Latitude:     "34.0522",
		Longitude:    "-118.2437",
		Status:       "available",
	}
	createResponse, err := resourceRepo.CreateResource(context.Background(), testResource)
	assert.NoError(t, err, "Creating resource for DeleteResource test failed")
	assert.NotEmpty(t, createResponse.ResourceId, "Created resource should have a valid ID")

	// 2. Delete the resource
	deleteResourceRequest := &emergency.DeleteResourceRequest{ResourceId: createResponse.ResourceId}
	deleteResponse, err := resourceRepo.DeleteResource(context.Background(), deleteResourceRequest)

	// Assertions for DeleteResource
	assert.NoError(t, err, "DeleteResource should not return an error")
	assert.NotNil(t, deleteResponse, "DeleteResource response should not be nil")
	assert.Equal(t, "success", deleteResponse.Status, "DeleteResource should return success status")

	// 3. Attempt to retrieve the deleted resource (should fail)
	getResourceRequest := &emergency.GetResourceRequest{ResourceId: createResponse.ResourceId}
	getResponse, err := resourceRepo.GetResource(context.Background(), getResourceRequest)
	assert.Error(t, err, "GetResource after delete should return an error")
	assert.Nil(t, getResponse, "GetResource response should be nil after delete")
}

func TestUpdateResourceStatus(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)
	defer db.Close(context.Background())

	resourceRepo := postgres.NewResource(db)

	// 1. Create a resource to update its status
	testResource := &emergency.CreateResourceRequest{
		ResourceType: "Ambulance",
		Latitude:     "34.0522",
		Longitude:    "-118.2437",
		Status:       "available", // Initial status
	}
	createResponse, err := resourceRepo.CreateResource(context.Background(), testResource)
	assert.NoError(t, err, "Creating resource for UpdateResourceStatus test failed")
	assert.NotEmpty(t, createResponse.ResourceId, "Created resource should have a valid ID")

	// 2. Update the resource's status
	updateStatusRequest := &emergency.UpdateResourceStatusRequest{
		ResourceId: createResponse.ResourceId,
		Status:     "dispatched", // New status
	}
	updateResponse, err := resourceRepo.UpdateResourceStatus(context.Background(), updateStatusRequest)

	// Assertions for UpdateResourceStatus
	assert.NoError(t, err, "UpdateResourceStatus should not return an error")
	assert.NotNil(t, updateResponse, "UpdateResourceStatus response should not be nil")
	assert.Equal(t, "success", updateResponse.Status, "UpdateResourceStatus should return success status")

	// 3. Retrieve the resource to verify the status update
	getResourceRequest := &emergency.GetResourceRequest{ResourceId: createResponse.ResourceId}
	getResponse, err := resourceRepo.GetResource(context.Background(), getResourceRequest)
	assert.NoError(t, err, "GetResource after status update should not return an error")
	assert.Equal(t, updateStatusRequest.Status, getResponse.Resource.Status, "Resource status should be updated")

	// Clean up
	_, err = db.Exec(context.Background(), "DELETE FROM emergency_resources WHERE resource_id = $1", createResponse.ResourceId)
	assert.NoError(t, err, "Cleanup: failed to delete test resource")
}

func TestListAvailableResources(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)
	defer db.Close(context.Background())

	resourceRepo := postgres.NewResource(db)

	// 1. Create some resources, with at least two being "available" and of the same emergency type
	testResources := []*emergency.CreateResourceRequest{
		{ResourceType: "Ambulance", Latitude: "34.0522", Longitude: "-118.2437", Status: "available"},
		{ResourceType: "Fire Truck", Latitude: "37.7749", Longitude: "-122.4194", Status: "dispatched"},
		{ResourceType: "Ambulance", Latitude: "40.7128", Longitude: "-74.0060", Status: "available"},
		{ResourceType: "Police Car", Latitude: "41.8781", Longitude: "-87.6298", Status: "available"},
	}

	createdResourceIds := make([]string, 0) // To store IDs for cleanup
	for _, res := range testResources {
		createResponse, err := resourceRepo.CreateResource(context.Background(), res)
		assert.NoError(t, err, "Creating resource for ListAvailableResources test failed")
		assert.NotEmpty(t, createResponse.ResourceId, "Created resource should have a valid ID")
		createdResourceIds = append(createdResourceIds, createResponse.ResourceId)
	}

	// 2. List available resources (filter by emergency type "Ambulance")
	listRequest := &emergency.ListAvailableResourcesRequest{EmergencyType: "Ambulance"}
	listResponse, err := resourceRepo.ListAvailableResources(context.Background(), listRequest)

	// Assertions for ListAvailableResources
	assert.NoError(t, err, "ListAvailableResources should not return an error")
	assert.NotNil(t, listResponse, "ListAvailableResources response should not be nil")
	assert.GreaterOrEqual(t, len(listResponse.Resources), 2, "Should have at least two available ambulances")

	// Clean up
	for _, id := range createdResourceIds {
		_, err = db.Exec(context.Background(), "DELETE FROM emergency_resources WHERE resource_id = $1", id)
		assert.NoError(t, err, "Cleanup: failed to delete test resource")
	}
}
