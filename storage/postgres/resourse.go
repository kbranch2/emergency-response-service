package postgres

import (
	"context"
	"errors"
	"fmt"

	"github.com/jackc/pgx/v5"
	"github.com/smart-city/emergency-response-service/genproto/emergency"
)

type ResourceRepo struct {
	Db *pgx.Conn
}

func NewResource(db *pgx.Conn) *ResourceRepo {
	return &ResourceRepo{Db: db}
}

func (resR *ResourceRepo) CreateResource(ctx context.Context, req *emergency.CreateResourceRequest) (*emergency.CreateResourceResponse, error) {
	var resourceID string
	err := resR.Db.QueryRow(ctx,
		`INSERT INTO 
			emergency_resources (
				resource_type, 
				current_location, 
				status
			) 
		 VALUES (
		 	$1, 
			ST_SetSRID(ST_MakePoint($2, $3), 4326), 
			$4
		) 
		RETURNING 
			resource_id;`,
		req.ResourceType, req.Longitude, req.Latitude, req.Status).Scan(&resourceID) // Note: Longitude comes before Latitude in ST_MakePoint

	if err != nil {
		return nil, fmt.Errorf("error creating resource: %w", err)
	}

	return &emergency.CreateResourceResponse{
		ResourceId: resourceID,
	}, nil
}

func (resR *ResourceRepo) GetResource(ctx context.Context, req *emergency.GetResourceRequest) (*emergency.GetResourceResponse, error) {
	resource := &emergency.EmergencyResource{}
	var lat, long float64
	err := resR.Db.QueryRow(ctx,
		`SELECT 
			resource_id, 
			resource_type, 
			ST_X(current_location) AS longitude, 
			ST_Y(current_location) AS latitude, 
			status 
		FROM 
			emergency_resources 
		WHERE 
			resource_id = $1;`,
		req.ResourceId).Scan(&resource.ResourceId, &resource.ResourceType, &long, &lat, &resource.Status)
	if err != nil {
		return nil, fmt.Errorf("error getting resource: %w", err)
	}

	// Assigning latitude and longitude to the resource
	resource.Latitude = fmt.Sprintf("%f", lat)
	resource.Longitude = fmt.Sprintf("%f", long)

	return &emergency.GetResourceResponse{
		Resource: resource,
	}, nil
}

func (resR *ResourceRepo) UpdateResource(ctx context.Context, req *emergency.UpdateResourceRequest) (*emergency.UpdateResourceResponse, error) {
	if req.Latitude == "" || req.Longitude == "" {
		return nil, errors.New("the location must have x and y points")
	}
	commandTag, err := resR.Db.Exec(ctx,
		`UPDATE 
			emergency_resources 
		SET 
			resource_type = $1, 
			current_location = ST_SetSRID(ST_MakePoint($2, $3), 4326), 
			status = $4 
		WHERE resource_id = $5;`,
		req.ResourceType, req.Longitude, req.Latitude, req.Status, req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("error updating resource: %w", err)
	}

	if commandTag.RowsAffected() == 0 {
		return &emergency.UpdateResourceResponse{Status: "error: resource not found"}, nil
	}

	return &emergency.UpdateResourceResponse{Status: "success"}, nil
}

func (resR *ResourceRepo) DeleteResource(ctx context.Context, req *emergency.DeleteResourceRequest) (*emergency.DeleteResourceResponse, error) {
	commandTag, err := resR.Db.Exec(ctx,
		`DELETE FROM 
			emergency_resources 
		WHERE 
			resource_id = $1;`,
		req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("error deleting resource: %w", err)
	}

	if commandTag.RowsAffected() == 0 {
		return &emergency.DeleteResourceResponse{Status: "error: resource not found"}, nil
	}
	return &emergency.DeleteResourceResponse{Status: "success"}, nil
}

func (resR *ResourceRepo) UpdateResourceStatus(ctx context.Context, req *emergency.UpdateResourceStatusRequest) (*emergency.UpdateResourceStatusResponse, error) {
	commandTag, err := resR.Db.Exec(ctx,
		`UPDATE 
			emergency_resources 
		SET 
			status = $1
		WHERE 
			resource_id = $2;`,
		req.Status, req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("error updating resource status: %w", err)
	}

	if commandTag.RowsAffected() == 0 {
		return &emergency.UpdateResourceStatusResponse{Status: "error: resource not found"}, nil
	}

	return &emergency.UpdateResourceStatusResponse{Status: "success"}, nil
}

func (resR *ResourceRepo) ListAvailableResources(ctx context.Context, req *emergency.ListAvailableResourcesRequest) (*emergency.ListAvailableResourcesResponse, error) {
	rows, err := resR.Db.Query(ctx,
		`SELECT 
			resource_id, 
			resource_type, 
			ST_X(current_location) AS longitude, 
			ST_Y(current_location) AS latitude, 
			status 
		FROM 
		 	emergency_resources 
		WHERE 
			status = 'available' 
		AND 
			resource_type = $1;`,
		req.EmergencyType)
	if err != nil {
		return nil, fmt.Errorf("error listing available resources: %w", err)
	}
	defer rows.Close()

	var resources []*emergency.EmergencyResource
	for rows.Next() {
		resource := &emergency.EmergencyResource{}
		var lat, long float64
		err := rows.Scan(&resource.ResourceId, &resource.ResourceType, &long, &lat, &resource.Status)
		if err != nil {
			return nil, fmt.Errorf("error scanning resource: %w", err)
		}

		// Assigning latitude and longitude to the resource
		resource.Latitude = fmt.Sprintf("%f", lat)
		resource.Longitude = fmt.Sprintf("%f", long)

		resources = append(resources, resource)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("error iterating resources: %w", err)
	}

	return &emergency.ListAvailableResourcesResponse{
		Resources: resources,
	}, nil
}
