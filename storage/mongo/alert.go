package mongodb

import (
	"context"
	"fmt"
	"log"
	"time"

	geojson "github.com/paulmach/go.geojson"
	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type AlertRepo struct {
	DB *mongo.Database
}

func NewAlert(db *mongo.Database) *AlertRepo {
	return &AlertRepo{DB: db}
}
func (a *AlertRepo) CreateAlert(ctx context.Context, req *emergency.CreateAlertRequest) (*emergency.CreateAlertResponse, error) {
	// Construct a GeoJSON Polygon from the affected_area_points
	var polygonCoordinates [][]float64
	for _, pointStr := range req.AffectedAreaPoints {
		var lat, long float64
		_, err := fmt.Sscanf(pointStr, "%f,%f", &lat, &long)
		if err != nil {
			return nil, fmt.Errorf("invalid affected area point: %s", pointStr)
		}
		polygonCoordinates = append(polygonCoordinates, []float64{long, lat})
	}
	// Close the polygon (repeat the first point)
	polygonCoordinates = append(polygonCoordinates, polygonCoordinates[0])

	polygon := geojson.NewPolygonGeometry([][][]float64{polygonCoordinates})
	log.Println(polygon, "Hello")
	newAlert := bson.M{
		"alert_type":    req.AlertType,
		"message":       req.Message,
		"affected_area": polygon, // Store the GeoJSON Polygon
		"issued_at":     time.Now(),
	}

	result, err := a.DB.Collection("alerts").InsertOne(ctx, newAlert)
	if err != nil {
		return nil, fmt.Errorf("error creating alert: %w", err)
	}

	alertID := result.InsertedID.(primitive.ObjectID).Hex()
	return &emergency.CreateAlertResponse{AlertId: alertID}, nil
}
