package mongodb

import (
	"context"

	"github.com/smart-city/emergency-response-service/storage"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type StorageRepo struct {
	Db         *mongo.Database
	IncidentS  storage.IncidentI
	ResuourceS storage.ResourceI
	AlertS     storage.AlertI
	DispatchS  storage.DispatchI
}

func DbConn() (*StorageRepo, error) {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		return nil, err
	}
	db := client.Database("emergency")
	incident := NewIncident(db)
	recourse := NewResource(db)
	alert := NewAlert(db)
	dispatch := NewDispatch(db)
	return &StorageRepo{
		IncidentS:  incident,
		ResuourceS: recourse,
		AlertS:     alert,
		DispatchS:  dispatch,
	}, nil

}

func (st *StorageRepo) Incident() storage.IncidentI {
	if st.IncidentS == nil {
		st.IncidentS = NewIncident(st.Db)
	}
	return st.IncidentS
}
func (st *StorageRepo) Resource() storage.ResourceI {
	if st.ResuourceS == nil {
		st.ResuourceS = NewResource(st.Db)
	}
	return st.ResuourceS
}
func (st *StorageRepo) Dispatch() storage.DispatchI {
	if st.DispatchS == nil {
		st.DispatchS = NewDispatch(st.Db)
	}
	return st.DispatchS
}
func (st *StorageRepo) Alert() storage.AlertI {
	if st.AlertS == nil {
		st.AlertS = NewAlert(st.Db)
	}
	return st.AlertS
}
