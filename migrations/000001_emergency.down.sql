DROP TABLE IF EXISTS resource_dispatches;
DROP TABLE IF EXISTS emergency_alerts;
DROP TABLE IF EXISTS emergency_resources;
DROP TABLE IF EXISTS emergency_incidents;

DROP EXTENSION IF EXISTS "uuid-ossp";