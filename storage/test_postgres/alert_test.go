package test

import (
	"context"
	"fmt"
	"testing"

	"github.com/jackc/pgx/v5"
	"github.com/smart-city/emergency-response-service/config"
	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"github.com/smart-city/emergency-response-service/storage/postgres"

	"github.com/stretchr/testify/assert"
)

// Function to establish database connection
func createDBConnection(t *testing.T) *pgx.Conn {
	cfg := config.Load()
	dbCon := fmt.Sprintf("postgresql://%s:%s@%s:%d/%s",
		cfg.DB_USER,
		cfg.DB_PASSWORD,
		cfg.DB_HOST,
		uint16(cfg.DB_PORT),
		cfg.DB_NAME,
	)

	// Connecting to postgres
	db, err := pgx.Connect(context.Background(), dbCon)
	if err != nil {
		t.Fatalf("Unable to connect to database: %v", err)
	}
	return db
}

func TestCreateAlert(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)
	defer db.Close(context.Background())

	// Create an instance of AlertRepo
	alertRepo := postgres.NewAlert(db)

	// Test data
	testAlert := &emergency.CreateAlertRequest{
		AlertType:          "Fire",
		Message:            "Fire reported in the building.",
		AffectedAreaPoints: []string{"34.0522,-118.2437", "34.0522,-118.2440", "34.0525,-118.2440", "34.0525,-118.2437", "34.0522,-118.2437"},
	}

	// Call the CreateAlert method
	response, err := alertRepo.CreateAlert(context.Background(), testAlert)

	// Assertions
	assert.NoError(t, err, "CreateAlert should not return an error")
	assert.NotNil(t, response, "CreateAlert response should not be nil")
	assert.NotEmpty(t, response.AlertId, "Created alert should have a valid ID")

	// Clean up the created alert
	_, err = db.Exec(context.Background(), "DELETE FROM emergency_alerts WHERE alert_id = $1", response.AlertId)
	assert.NoError(t, err, "Cleanup: failed to delete test alert")
}
