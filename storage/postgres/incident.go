package postgres

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/smart-city/emergency-response-service/genproto/emergency"
)

type IncidentRepo struct {
	Db *pgx.Conn
}

func NewIncident(db *pgx.Conn) *IncidentRepo {
	return &IncidentRepo{Db: db}
}

func (incR *IncidentRepo) CreateIncident(ctx context.Context, req *emergency.CreateIncidentRequest) (*emergency.CreateIncidentResponse, error) {
	var incidentID string

	query := `
		INSERT INTO 
			emergency_incidents (
				incident_type, 
				location, 
				description, 
				status
			) 
		VALUES (
			$1, 
			ST_SetSRID(ST_MakePoint($2, $3), 4326), 
			$4, 
			'active') 
		RETURNING 
			incident_id;
	`

	err := incR.Db.QueryRow(ctx, query,
		req.IncidentType, req.Longitude, req.Latitude, req.Description,
	).Scan(&incidentID)

	if err != nil {
		return nil, fmt.Errorf("error creating incident: %w", err)
	}

	return &emergency.CreateIncidentResponse{
		IncidentId: incidentID,
	}, nil
}

func (incR *IncidentRepo) GetIncident(ctx context.Context, req *emergency.GetIncidentRequest) (*emergency.GetIncidentResponse, error) {
	incident := &emergency.EmergencyIncident{}
	var (
		lat, long   float64
		reported_at time.Time
	)

	query := `
		SELECT 
			incident_id, 
			incident_type, 
		    ST_X(location) AS longitude, 
			ST_Y(location) AS latitude, 
		    description, 
			status, 
			reported_at
		FROM 
			emergency_incidents 
		WHERE 
			incident_id = $1;
	`

	err := incR.Db.QueryRow(ctx, query, req.IncidentId).Scan(
		&incident.IncidentId, &incident.IncidentType, &long, &lat,
		&incident.Description, &incident.Status, &reported_at,
	)
	if err != nil {
		return nil, fmt.Errorf("error getting incident: %w", err)
	}
	incident.ReportedAt = reported_at.String()
	incident.Latitude = fmt.Sprintf("%f", lat)
	incident.Longitude = fmt.Sprintf("%f", long)

	return &emergency.GetIncidentResponse{
		Incident: incident,
	}, nil
}

func (incR *IncidentRepo) ListActiveIncidents(ctx context.Context, req *emergency.ListActiveIncidentsRequest) (*emergency.ListActiveIncidentsResponse, error) {
	query := `
		SELECT 
			incident_id, 
			incident_type, 
		    ST_X(location) AS longitude, 
			ST_Y(location) AS latitude, 
		    description, 
			status, 
			reported_at
		FROM 
			emergency_incidents 
		WHERE 
			status = 'active';
	`

	rows, err := incR.Db.Query(ctx, query)
	if err != nil {
		return nil, fmt.Errorf("error listing active incidents: %w", err)
	}
	defer rows.Close()

	var incidents []*emergency.EmergencyIncident
	for rows.Next() {
		incident := &emergency.EmergencyIncident{}
		var (
			lat, long   float64
			reported_at time.Time
		)

		err := rows.Scan(
			&incident.IncidentId, &incident.IncidentType, &long, &lat,
			&incident.Description, &incident.Status, &reported_at,
		)
		if err != nil {
			return nil, fmt.Errorf("error scanning incident: %w", err)
		}
		incident.ReportedAt = reported_at.String()
		incident.Latitude = fmt.Sprintf("%f", lat)
		incident.Longitude = fmt.Sprintf("%f", long)

		incidents = append(incidents, incident)
	}

	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("error iterating incidents: %w", err)
	}

	return &emergency.ListActiveIncidentsResponse{
		Incidents: incidents,
	}, nil
}

func (incR *IncidentRepo) UpdateIncidentStatus(ctx context.Context, req *emergency.UpdateIncidentStatusRequest) (*emergency.UpdateIncidentStatusResponse, error) {
	commandTag, err := incR.Db.Exec(ctx,
		`UPDATE emergency_incidents SET status = $1 WHERE incident_id = $2;`,
		req.Status, req.IncidentId)
	if err != nil {
		return nil, fmt.Errorf("error updating incident status: %w", err)
	}

	if commandTag.RowsAffected() == 0 {
		return &emergency.UpdateIncidentStatusResponse{Status: "error: incident not found"}, nil
	}

	return &emergency.UpdateIncidentStatusResponse{Status: "success"}, nil
}

func (incR *IncidentRepo) DeleteIncident(ctx context.Context, req *emergency.DeleteIncidentRequest) (*emergency.DeleteIncidentResponse, error) {
	commandTag, err := incR.Db.Exec(ctx,
		`DELETE FROM emergency_incidents WHERE incident_id = $1;`,
		req.IncidentId)
	if err != nil {
		return nil, fmt.Errorf("error deleting incident: %w", err)
	}

	if commandTag.RowsAffected() == 0 {
		return &emergency.DeleteIncidentResponse{Status: "error: incident not found"}, nil
	}
	return &emergency.DeleteIncidentResponse{Status: "success"}, nil
}
