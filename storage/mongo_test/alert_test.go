package test

import (
	"context"
	"testing"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	mongodb "github.com/smart-city/emergency-response-service/storage/mongo"
	"github.com/stretchr/testify/assert"
)

func TestCreateAlert(t *testing.T) {
	// Get database connection
	db := createDBConnection(t)

	// Create an instance of AlertRepo
	alertRepo := mongodb.NewAlert(db)

	// Test data
	testAlert := &emergency.CreateAlertRequest{
		AlertType:          "Fire",
		Message:            "Fire reported in the building.",
		AffectedAreaPoints: []string{"34.0522,-118.2437", "34.0522,-118.2440", "34.0525,-118.2440", "34.0525,-118.2437", "34.0522,-118.2437"},
	}

	// Call the CreateAlert method
	response, err := alertRepo.CreateAlert(context.Background(), testAlert)

	// Assertions
	assert.NoError(t, err, "CreateAlert should not return an error")
	assert.NotNil(t, response, "CreateAlert response should not be nil")
	assert.NotEmpty(t, response.AlertId, "Created alert should have a valid ID")

}
