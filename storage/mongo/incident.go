package mongodb

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type IncidentRepo struct {
	DB *mongo.Database
}

func NewIncident(db *mongo.Database) *IncidentRepo {
	return &IncidentRepo{DB: db}
}

func (incR *IncidentRepo) CreateIncident(ctx context.Context, req *emergency.CreateIncidentRequest) (*emergency.CreateIncidentResponse, error) {
	newIncident := bson.M{
		"incident_type": req.IncidentType,
		"latitude":      req.Latitude,
		"longitude":     req.Longitude,
		"description":   req.Description,
		"status":        "active", // Set initial status to "active"
		"reported_at":   time.Now(),
	}

	result, err := incR.DB.Collection("incidents").InsertOne(ctx, newIncident)
	if err != nil {
		return nil, fmt.Errorf("error creating incident: %w", err)
	}

	incidentID := result.InsertedID.(primitive.ObjectID).Hex()
	return &emergency.CreateIncidentResponse{IncidentId: incidentID}, nil
}

func (incR *IncidentRepo) GetIncident(ctx context.Context, req *emergency.GetIncidentRequest) (*emergency.GetIncidentResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.IncidentId)
	if err != nil {
		return nil, fmt.Errorf("invalid incident ID: %w", err)
	}

	var incident emergency.EmergencyIncident
	err = incR.DB.Collection("incidents").FindOne(ctx, bson.M{"_id": objID}).Decode(&incident)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, status.Errorf(codes.NotFound, "incident not found")
		}
		return nil, fmt.Errorf("error getting incident: %w", err)
	}

	return &emergency.GetIncidentResponse{Incident: &incident}, nil
}

func (incR *IncidentRepo) ListActiveIncidents(ctx context.Context, req *emergency.ListActiveIncidentsRequest) (*emergency.ListActiveIncidentsResponse, error) {
	filter := bson.M{"status": "active"}

	cursor, err := incR.DB.Collection("incidents").Find(ctx, filter)
	if err != nil {
		return nil, fmt.Errorf("error finding active incidents: %w", err)
	}
	defer cursor.Close(ctx)

	var incidents []*emergency.EmergencyIncident
	if err = cursor.All(ctx, &incidents); err != nil {
		return nil, fmt.Errorf("error decoding active incidents: %w", err)
	}

	return &emergency.ListActiveIncidentsResponse{Incidents: incidents}, nil
}

func (incR *IncidentRepo) UpdateIncidentStatus(ctx context.Context, req *emergency.UpdateIncidentStatusRequest) (*emergency.UpdateIncidentStatusResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.IncidentId)
	if err != nil {
		return nil, fmt.Errorf("invalid incident ID: %w", err)
	}

	update := bson.M{
		"$set": bson.M{"status": req.Status},
	}

	result, err := incR.DB.Collection("incidents").UpdateOne(ctx, bson.M{"_id": objID}, update)
	if err != nil {
		return nil, fmt.Errorf("error updating incident status: %w", err)
	}

	if result.ModifiedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "incident not found")
	}

	return &emergency.UpdateIncidentStatusResponse{Status: "success"}, nil
}

func (incR *IncidentRepo) DeleteIncident(ctx context.Context, req *emergency.DeleteIncidentRequest) (*emergency.DeleteIncidentResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.IncidentId)
	if err != nil {
		return nil, fmt.Errorf("invalid incident ID: %w", err)
	}

	result, err := incR.DB.Collection("incidents").DeleteOne(ctx, bson.M{"_id": objID})
	if err != nil {
		return nil, fmt.Errorf("error deleting incident: %w", err)
	}

	if result.DeletedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "incident not found")
	}

	return &emergency.DeleteIncidentResponse{Status: "success"}, nil
}
