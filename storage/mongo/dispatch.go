package mongodb

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DispatchRepo struct {
	DB *mongo.Database
}

func NewDispatch(db *mongo.Database) *DispatchRepo {
	return &DispatchRepo{DB: db}
}

func (d *DispatchRepo) DispatchResource(ctx context.Context, req *emergency.DispatchResourceRequest) (*emergency.DispatchResourceResponse, error) {
	// You likely want to check if the incident and resource exist before dispatching
	// (similar to how it was done in the PostgreSQL version)

	newDispatch := bson.M{
		"incident_id":   req.IncidentId,
		"resource_id":   req.ResourceId,
		"dispatched_at": time.Now(),
		"arrived_at":    nil, // Initially nil, will be set when the resource arrives
	}

	result, err := d.DB.Collection("dispatches").InsertOne(ctx, newDispatch)
	if err != nil {
		return nil, fmt.Errorf("error creating dispatch record: %w", err)
	}

	dispatchID := result.InsertedID.(primitive.ObjectID).Hex()
	return &emergency.DispatchResourceResponse{DispatchId: dispatchID}, nil
}

func (d *DispatchRepo) GetDispatch(ctx context.Context, req *emergency.GetDispatchRequest) (*emergency.GetDispatchResponse, error) {

	objID, err := primitive.ObjectIDFromHex(req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("invalid dispatch ID: %w", err)
	}

	var dispatch struct {
		DispatchId   string    `protobuf:"bytes,1,opt,name=dispatch_id,json=dispatchId,proto3" json:"dispatch_id,omitempty" bson:"dispatch_id"` // UUID
		IncidentId   string    `protobuf:"bytes,2,opt,name=incident_id,json=incidentId,proto3" json:"incident_id,omitempty" bson:"incident_id"` // UUID
		ResourceId   string    `protobuf:"bytes,3,opt,name=resource_id,json=resourceId,proto3" json:"resource_id,omitempty" bson:"resource_id"` // UUID
		DispatchedAt time.Time `protobuf:"bytes,4,opt,name=dispatched_at,json=dispatchedAt,proto3" json:"dispatched_at,omitempty" bson:"dispatched_at"`
		ArrivedAt    time.Time `protobuf:"bytes,5,opt,name=arrived_at,json=arrivedAt,proto3" json:"arrived_at,omitempty" bson:"arrived_at"`
	}
	err = d.DB.Collection("dispatches").FindOne(ctx, bson.M{"_id": objID}).Decode(&dispatch)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, status.Errorf(codes.NotFound, "dispatch not found")
		}
		return nil, fmt.Errorf("error getting dispatch: %w", err)
	}

	return &emergency.GetDispatchResponse{Dispatch: &emergency.ResourceDispatch{
		DispatchId:   dispatch.DispatchId,
		IncidentId:   dispatch.IncidentId,
		ResourceId:   dispatch.ResourceId,
		DispatchedAt: dispatch.DispatchedAt.String(),
		ArrivedAt:    dispatch.ArrivedAt.String(),
	}}, nil
}

func (d *DispatchRepo) UpdateDispatch(ctx context.Context, req *emergency.UpdateDispatchRequest) (*emergency.UpdateDispatchResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("invalid dispatch ID: %w", err)
	}

	// Note: You might want to avoid updating dispatched_at.
	// Handle corrections in dispatch time separately.

	update := bson.M{
		"$set": bson.M{
			"incident_id": req.IncidentId,
			"resource_id": req.ResourceId,
			"arrived_at":  req.ArrivedAt,
		},
	}

	result, err := d.DB.Collection("dispatches").UpdateOne(ctx, bson.M{"_id": objID}, update)
	if err != nil {
		return nil, fmt.Errorf("error updating dispatch: %w", err)
	}

	if result.ModifiedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "dispatch not found")
	}

	return &emergency.UpdateDispatchResponse{Status: "success"}, nil
}

func (d *DispatchRepo) DeleteDispatch(ctx context.Context, req *emergency.DeleteDispatchRequest) (*emergency.DeleteDispatchResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("invalid dispatch ID: %w", err)
	}

	result, err := d.DB.Collection("dispatches").DeleteOne(ctx, bson.M{"_id": objID})
	if err != nil {
		return nil, fmt.Errorf("error deleting dispatch: %w", err)
	}

	if result.DeletedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "dispatch not found")
	}

	return &emergency.DeleteDispatchResponse{Status: "success"}, nil
}

func (d *DispatchRepo) ArriveResource(ctx context.Context, req *emergency.ArriveResourceRequest) (*emergency.ArriveResourceResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("invalid dispatch ID: %w", err)
	}

	update := bson.M{
		"$set": bson.M{
			"arrived_at": time.Now(),
		},
	}

	result, err := d.DB.Collection("dispatches").UpdateOne(ctx, bson.M{"_id": objID}, update)
	if err != nil {
		return nil, fmt.Errorf("error updating dispatch with arrival time: %w", err)
	}

	if result.ModifiedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "dispatch not found")
	}

	return &emergency.ArriveResourceResponse{Status: "success"}, nil
}
