package mongodb

import (
	"context"
	"errors"
	"fmt"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ResourceRepo struct {
	DB *mongo.Database
}

func NewResource(db *mongo.Database) *ResourceRepo {
	return &ResourceRepo{DB: db}
}

func (resR *ResourceRepo) CreateResource(ctx context.Context, req *emergency.CreateResourceRequest) (*emergency.CreateResourceResponse, error) {
	newResource := bson.M{
		"resource_type": req.ResourceType,
		"latitude":      req.Latitude,
		"longitude":     req.Longitude,
		"status":        req.Status,
	}

	result, err := resR.DB.Collection("resources").InsertOne(ctx, newResource)
	if err != nil {
		return nil, fmt.Errorf("error creating resource: %w", err)
	}

	resourceID := result.InsertedID.(primitive.ObjectID).Hex()
	return &emergency.CreateResourceResponse{ResourceId: resourceID}, nil
}

func (resR *ResourceRepo) GetResource(ctx context.Context, req *emergency.GetResourceRequest) (*emergency.GetResourceResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("invalid resource ID: %w", err)
	}

	var resource emergency.EmergencyResource
	err = resR.DB.Collection("resources").FindOne(ctx, bson.M{"_id": objID}).Decode(&resource)
	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return nil, status.Errorf(codes.NotFound, "resource not found")
		}
		return nil, fmt.Errorf("error getting resource: %w", err)
	}

	return &emergency.GetResourceResponse{Resource: &resource}, nil
}

func (resR *ResourceRepo) UpdateResource(ctx context.Context, req *emergency.UpdateResourceRequest) (*emergency.UpdateResourceResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("invalid resource ID: %w", err)
	}

	update := bson.M{
		"$set": bson.M{
			"resource_type": req.ResourceType,
			"latitude":      req.Latitude,
			"longitude":     req.Longitude,
			"status":        req.Status,
		},
	}

	result, err := resR.DB.Collection("resources").UpdateOne(ctx, bson.M{"_id": objID}, update)
	if err != nil {
		return nil, fmt.Errorf("error updating resource: %w", err)
	}

	if result.ModifiedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "resource not found")
	}

	return &emergency.UpdateResourceResponse{Status: "success"}, nil
}

func (resR *ResourceRepo) DeleteResource(ctx context.Context, req *emergency.DeleteResourceRequest) (*emergency.DeleteResourceResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("invalid resource ID: %w", err)
	}

	result, err := resR.DB.Collection("resources").DeleteOne(ctx, bson.M{"_id": objID})
	if err != nil {
		return nil, fmt.Errorf("error deleting resource: %w", err)
	}

	if result.DeletedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "resource not found")
	}

	return &emergency.DeleteResourceResponse{Status: "success"}, nil
}

func (resR *ResourceRepo) UpdateResourceStatus(ctx context.Context, req *emergency.UpdateResourceStatusRequest) (*emergency.UpdateResourceStatusResponse, error) {
	objID, err := primitive.ObjectIDFromHex(req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("invalid resource ID: %w", err)
	}

	update := bson.M{
		"$set": bson.M{"status": req.Status},
	}

	result, err := resR.DB.Collection("resources").UpdateOne(ctx, bson.M{"_id": objID}, update)
	if err != nil {
		return nil, fmt.Errorf("error updating resource status: %w", err)
	}

	if result.ModifiedCount == 0 {
		return nil, status.Errorf(codes.NotFound, "resource not found")
	}

	return &emergency.UpdateResourceStatusResponse{Status: "success"}, nil
}

func (resR *ResourceRepo) ListAvailableResources(ctx context.Context, req *emergency.ListAvailableResourcesRequest) (*emergency.ListAvailableResourcesResponse, error) {
	filter := bson.M{
		"status":        "available",
		"resource_type": req.EmergencyType,
	}

	cursor, err := resR.DB.Collection("resources").Find(ctx, filter)
	if err != nil {
		return nil, fmt.Errorf("error finding available resources: %w", err)
	}
	defer cursor.Close(ctx)

	var resources []*emergency.EmergencyResource
	if err = cursor.All(ctx, &resources); err != nil {
		return nil, fmt.Errorf("error decoding available resources: %w", err)
	}

	return &emergency.ListAvailableResourcesResponse{Resources: resources}, nil
}
