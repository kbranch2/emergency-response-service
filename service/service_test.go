package service

import (
	"context"
	"fmt"
	"log"
	"testing"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"github.com/smart-city/emergency-response-service/storage/postgres"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func NewStorage() *EmergencyResponseService {
	db, err := postgres.DbConn()
	if err != nil {
		log.Fatal(err)
	}
	return NewEmergencyResponseService(db)
}

func TestEmergencyResponseService_Methods(t *testing.T) {
	service := NewStorage() // Create a service using your real database connection

	// ------------------ Incident Methods ------------------
	t.Run("CreateIncident", func(t *testing.T) {
		req := &emergency.CreateIncidentRequest{
			IncidentType: "Fire",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Description:  "Test Fire Incident",
		}
		resp, err := service.CreateIncident(context.Background(), req)

		assert.NoError(t, err, "CreateIncident should not return an error")
		assert.NotNil(t, resp, "CreateIncident response should not be nil")
		assert.NotEmpty(t, resp.IncidentId, "IncidentId should be generated")

		// Clean up (delete the created incident)
		defer cleanUpIncident(t, service, resp.IncidentId)
	})

	t.Run("GetIncident", func(t *testing.T) {
		// 1. Create an incident to retrieve
		createReq := &emergency.CreateIncidentRequest{
			IncidentType: "Medical",
			Latitude:     "37.7749",
			Longitude:    "-122.4194",
			Description:  "Test Medical Incident",
		}
		createResp, err := service.CreateIncident(context.Background(), createReq)
		assert.NoError(t, err, "Failed to create incident for GetIncident test")
		assert.NotEmpty(t, createResp.IncidentId, "IncidentId should be generated")

		// 2. Get the created incident
		getResp, err := service.GetIncident(context.Background(), &emergency.GetIncidentRequest{IncidentId: createResp.IncidentId})
		assert.NoError(t, err, "GetIncident should not return an error")
		assert.NotNil(t, getResp, "GetIncident response should not be nil")
		assert.Equal(t, createResp.IncidentId, getResp.Incident.IncidentId, "Incident IDs should match")

		// Clean up
		defer cleanUpIncident(t, service, createResp.IncidentId)
	})

	t.Run("ListActiveIncidents", func(t *testing.T) {
		// 1. Create at least two active incidents
		createReq1 := &emergency.CreateIncidentRequest{
			IncidentType: "Traffic",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Description:  "Test Traffic Incident 1",
		}
		createResp1, err := service.CreateIncident(context.Background(), createReq1)
		assert.NoError(t, err, "Failed to create incident 1 for ListActiveIncidents test")
		assert.NotEmpty(t, createResp1.IncidentId, "IncidentId 1 should be generated")

		createReq2 := &emergency.CreateIncidentRequest{
			IncidentType: "Fire",
			Latitude:     "37.7749",
			Longitude:    "-122.4194",
			Description:  "Test Fire Incident 2",
		}
		createResp2, err := service.CreateIncident(context.Background(), createReq2)
		assert.NoError(t, err, "Failed to create incident 2 for ListActiveIncidents test")
		assert.NotEmpty(t, createResp2.IncidentId, "IncidentId 2 should be generated")

		// 2. List active incidents
		listResp, err := service.ListActiveIncidents(context.Background(), &emergency.ListActiveIncidentsRequest{})
		assert.NoError(t, err, "ListActiveIncidents should not return an error")
		assert.NotNil(t, listResp, "ListActiveIncidents response should not be nil")
		assert.GreaterOrEqual(t, len(listResp.Incidents), 2, "Should be at least two active incidents")

		// Clean up
		defer cleanUpIncident(t, service, createResp1.IncidentId)
		defer cleanUpIncident(t, service, createResp2.IncidentId)
	})

	t.Run("UpdateIncidentStatus", func(t *testing.T) {
		// 1. Create an incident
		createReq := &emergency.CreateIncidentRequest{
			IncidentType: "Fire",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Description:  "Test Fire Incident for Status Update",
		}
		createResp, err := service.CreateIncident(context.Background(), createReq)
		assert.NoError(t, err, "Failed to create incident for UpdateIncidentStatus test")
		assert.NotEmpty(t, createResp.IncidentId, "IncidentId should be generated")

		// 2. Update the incident status
		updateResp, err := service.UpdateIncidentStatus(context.Background(), &emergency.UpdateIncidentStatusRequest{
			IncidentId: createResp.IncidentId,
			Status:     "resolved",
		})
		assert.NoError(t, err, "UpdateIncidentStatus should not return an error")
		assert.NotNil(t, updateResp, "UpdateIncidentStatus response should not be nil")
		assert.Equal(t, "success", updateResp.Status, "Status update should be successful")

		// 3. Verify the status update
		getResp, err := service.GetIncident(context.Background(), &emergency.GetIncidentRequest{IncidentId: createResp.IncidentId})
		assert.NoError(t, err, "GetIncident should not return an error")
		assert.Equal(t, "resolved", getResp.Incident.Status, "Incident status should be updated to 'resolved'")

		// Clean up
		defer cleanUpIncident(t, service, createResp.IncidentId)
	})

	t.Run("DeleteIncident", func(t *testing.T) {
		// 1. Create an incident
		createReq := &emergency.CreateIncidentRequest{
			IncidentType: "Fire",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Description:  "Test Fire Incident for Deletion",
		}
		createResp, err := service.CreateIncident(context.Background(), createReq)
		assert.NoError(t, err, "Failed to create incident for DeleteIncident test")
		assert.NotEmpty(t, createResp.IncidentId, "IncidentId should be generated")

		// 2. Delete the incident
		deleteResp, err := service.DeleteIncident(context.Background(), &emergency.DeleteIncidentRequest{IncidentId: createResp.IncidentId})
		assert.NoError(t, err, "DeleteIncident should not return an error")
		assert.NotNil(t, deleteResp, "DeleteIncident response should not be nil")
		assert.Equal(t, "success", deleteResp.Status, "Incident deletion should be successful")

		// 3. Verify that the incident is deleted
		_, err = service.GetIncident(context.Background(), &emergency.GetIncidentRequest{IncidentId: createResp.IncidentId})
		assert.Error(t, err, "GetIncident should return an error after deletion")
		assert.True(t, status.Code(err) == codes.NotFound, "Error code should be NotFound")
	})

	// ------------------ Resource Methods ------------------
	t.Run("CreateResource", func(t *testing.T) {
		req := &emergency.CreateResourceRequest{
			ResourceType: "Ambulance",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Status:       "available",
		}
		resp, err := service.CreateResource(context.Background(), req)

		assert.NoError(t, err, "CreateResource should not return an error")
		assert.NotNil(t, resp, "CreateResource response should not be nil")
		assert.NotEmpty(t, resp.ResourceId, "ResourceId should be generated")

		// Clean up (delete the created resource)
		defer cleanUpResource(t, service, resp.ResourceId)
	})

	t.Run("GetResource", func(t *testing.T) {
		// 1. Create a resource to retrieve
		createReq := &emergency.CreateResourceRequest{
			ResourceType: "Fire Truck",
			Latitude:     "37.7749",
			Longitude:    "-122.4194",
			Status:       "available",
		}
		createResp, err := service.CreateResource(context.Background(), createReq)
		assert.NoError(t, err, "Failed to create resource for GetResource test")
		assert.NotEmpty(t, createResp.ResourceId, "ResourceId should be generated")

		// 2. Get the created resource
		getResp, err := service.GetResource(context.Background(), &emergency.GetResourceRequest{ResourceId: createResp.ResourceId})
		assert.NoError(t, err, "GetResource should not return an error")
		assert.NotNil(t, getResp, "GetResource response should not be nil")
		assert.Equal(t, createResp.ResourceId, getResp.Resource.ResourceId, "Resource IDs should match")

		// Clean up
		defer cleanUpResource(t, service, createResp.ResourceId)
	})

	t.Run("UpdateResource", func(t *testing.T) {
		// 1. Create a resource
		createReq := &emergency.CreateResourceRequest{
			ResourceType: "Police Car",
			Latitude:     "40.7128",
			Longitude:    "-74.0060",
			Status:       "available",
		}
		createResp, err := service.CreateResource(context.Background(), createReq)
		assert.NoError(t, err, "Failed to create resource for UpdateResource test")
		assert.NotEmpty(t, createResp.ResourceId, "ResourceId should be generated")

		// 2. Update the resource
		updateReq := &emergency.UpdateResourceRequest{
			ResourceId:   createResp.ResourceId,
			ResourceType: "Ambulance",
			Latitude:     "41.8781",
			Longitude:    "-87.6298",
			Status:       "dispatched",
		}
		updateResp, err := service.UpdateResource(context.Background(), updateReq)
		assert.NoError(t, err, "UpdateResource should not return an error")
		assert.NotNil(t, updateResp, "UpdateResource response should not be nil")
		assert.Equal(t, "success", updateResp.Status, "Resource update should be successful")

		// 3. Verify the update
		getResp, err := service.GetResource(context.Background(), &emergency.GetResourceRequest{ResourceId: createResp.ResourceId})
		assert.NoError(t, err, "GetResource should not return an error")
		assert.Equal(t, "Ambulance", getResp.Resource.ResourceType, "ResourceType should be updated")
		assert.Equal(t, "dispatched", getResp.Resource.Status, "Status should be updated")
		// You can add more assertions for latitude and longitude here

		// Clean up
		defer cleanUpResource(t, service, createResp.ResourceId)
	})

	t.Run("DeleteResource", func(t *testing.T) {
		// 1. Create a resource
		createReq := &emergency.CreateResourceRequest{
			ResourceType: "Ambulance",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Status:       "available",
		}
		createResp, err := service.CreateResource(context.Background(), createReq)
		assert.NoError(t, err, "Failed to create resource for DeleteResource test")
		assert.NotEmpty(t, createResp.ResourceId, "ResourceId should be generated")

		// 2. Delete the resource
		deleteResp, err := service.DeleteResource(context.Background(), &emergency.DeleteResourceRequest{ResourceId: createResp.ResourceId})
		assert.NoError(t, err, "DeleteResource should not return an error")
		assert.NotNil(t, deleteResp, "DeleteResource response should not be nil")
		assert.Equal(t, "success", deleteResp.Status, "Resource deletion should be successful")

		// 3. Verify that the resource is deleted
		_, err = service.GetResource(context.Background(), &emergency.GetResourceRequest{ResourceId: createResp.ResourceId})
		assert.Error(t, err, "GetResource should return an error after deletion")
		assert.True(t, status.Code(err) == codes.NotFound, "Error code should be NotFound")
	})

	t.Run("UpdateResourceStatus", func(t *testing.T) {
		// 1. Create a resource
		createReq := &emergency.CreateResourceRequest{
			ResourceType: "Fire Truck",
			Latitude:     "37.7749",
			Longitude:    "-122.4194",
			Status:       "available",
		}
		createResp, err := service.CreateResource(context.Background(), createReq)
		assert.NoError(t, err, "Failed to create resource for UpdateResourceStatus test")
		assert.NotEmpty(t, createResp.ResourceId, "ResourceId should be generated")

		// 2. Update the resource's status
		updateResp, err := service.UpdateResourceStatus(context.Background(), &emergency.UpdateResourceStatusRequest{
			ResourceId: createResp.ResourceId,
			Status:     "unavailable",
		})
		assert.NoError(t, err, "UpdateResourceStatus should not return an error")
		assert.NotNil(t, updateResp, "UpdateResourceStatus response should not be nil")
		assert.Equal(t, "success", updateResp.Status, "Status update should be successful")

		// 3. Verify the status update
		getResp, err := service.GetResource(context.Background(), &emergency.GetResourceRequest{ResourceId: createResp.ResourceId})
		assert.NoError(t, err, "GetResource should not return an error")
		assert.Equal(t, "unavailable", getResp.Resource.Status, "Resource status should be updated to 'unavailable'")

		// Clean up
		defer cleanUpResource(t, service, createResp.ResourceId)
	})

	t.Run("ListAvailableResources", func(t *testing.T) {
		// 1. Create at least two resources of the same emergency type and mark them available
		createReq1 := &emergency.CreateResourceRequest{
			ResourceType: "Ambulance",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Status:       "available",
		}
		createResp1, err := service.CreateResource(context.Background(), createReq1)
		assert.NoError(t, err, "Failed to create resource 1 for ListAvailableResources test")
		assert.NotEmpty(t, createResp1.ResourceId, "ResourceId 1 should be generated")

		createReq2 := &emergency.CreateResourceRequest{
			ResourceType: "Ambulance",
			Latitude:     "37.7749",
			Longitude:    "-122.4194",
			Status:       "available",
		}
		createResp2, err := service.CreateResource(context.Background(), createReq2)
		assert.NoError(t, err, "Failed to create resource 2 for ListAvailableResources test")
		assert.NotEmpty(t, createResp2.ResourceId, "ResourceId 2 should be generated")

		// 2. List available resources of the specific emergency type
		listResp, err := service.ListAvailableResources(context.Background(), &emergency.ListAvailableResourcesRequest{
			EmergencyType: "Ambulance",
		})
		assert.NoError(t, err, "ListAvailableResources should not return an error")
		assert.NotNil(t, listResp, "ListAvailableResources response should not be nil")
		assert.GreaterOrEqual(t, len(listResp.Resources), 2, "Should be at least two available resources of type 'Ambulance'")

		// Clean up
		defer cleanUpResource(t, service, createResp1.ResourceId)
		defer cleanUpResource(t, service, createResp2.ResourceId)
	})

	// ------------------ Dispatch Methods ------------------
	t.Run("DispatchResource", func(t *testing.T) {
		// 1. Create an incident
		incidentReq := &emergency.CreateIncidentRequest{
			IncidentType: "Fire",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Description:  "Test Fire Incident for Dispatch",
		}
		incidentResp, err := service.CreateIncident(context.Background(), incidentReq)
		assert.NoError(t, err, "Failed to create incident for DispatchResource test")
		assert.NotEmpty(t, incidentResp.IncidentId, "IncidentId should be generated")

		// 2. Create a resource
		resourceReq := &emergency.CreateResourceRequest{
			ResourceType: "Fire Truck",
			Latitude:     "34.0500",
			Longitude:    "-118.2400",
			Status:       "available",
		}
		resourceResp, err := service.CreateResource(context.Background(), resourceReq)
		assert.NoError(t, err, "Failed to create resource for DispatchResource test")
		assert.NotEmpty(t, resourceResp.ResourceId, "ResourceId should be generated")

		// 3. Dispatch the resource
		dispatchReq := &emergency.DispatchResourceRequest{
			ResourceId: resourceResp.ResourceId,
			IncidentId: incidentResp.IncidentId,
		}
		dispatchResp, err := service.DispatchResource(context.Background(), dispatchReq)
		assert.NoError(t, err, "DispatchResource should not return an error")
		assert.NotNil(t, dispatchResp, "DispatchResource response should not be nil")
		assert.NotEmpty(t, dispatchResp.DispatchId, "DispatchId should be generated")

		// Clean up (delete dispatch, resource, and incident)
		defer cleanUpDispatch(t, service, dispatchResp.DispatchId, resourceResp.ResourceId, incidentResp.IncidentId)
	})

	t.Run("GetDispatch", func(t *testing.T) {
		// 1. Create incident, resource, and dispatch (similar to DispatchResource test)
		incident, resource := createTestIncidentAndResource(t, service)
		dispatchReq := &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		}
		dispatchResp, err := service.DispatchResource(context.Background(), dispatchReq)
		assert.NoError(t, err, "DispatchResource should not return an error")

		// 2. Get the dispatch
		getDispatchResp, err := service.GetDispatch(context.Background(), &emergency.GetDispatchRequest{DispatchId: dispatchResp.DispatchId})
		assert.NoError(t, err, "GetDispatch should not return an error")
		assert.NotNil(t, getDispatchResp, "GetDispatch response should not be nil")
		assert.Equal(t, dispatchResp.DispatchId, getDispatchResp.Dispatch.DispatchId, "Dispatch IDs should match")

		// Clean up
		defer cleanUpDispatch(t, service, dispatchResp.DispatchId, resource.ResourceId, incident.IncidentId)
	})

	t.Run("UpdateDispatch", func(t *testing.T) {
		// 1. Create incident, resource, and dispatch (similar to DispatchResource test)
		incident, resource := createTestIncidentAndResource(t, service)
		dispatchReq := &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		}
		dispatchResp, err := service.DispatchResource(context.Background(), dispatchReq)
		assert.NoError(t, err, "DispatchResource should not return an error")

		// 2. Update the dispatch (e.g., set arrived_at)
		updateReq := &emergency.UpdateDispatchRequest{
			IncidentId: dispatchReq.IncidentId,
			DispatchId: dispatchResp.DispatchId,
			ResourceId: dispatchReq.ResourceId,
			ArrivedAt:  "2024-02-29T15:00:00+05:00", // Example timestamp
		}
		updateResp, err := service.UpdateDispatch(context.Background(), updateReq)
		assert.NoError(t, err, "UpdateDispatch should not return an error")
		assert.Equal(t, "success", updateResp.Status, "UpdateDispatch should be successful")

		// 3. Verify the update
		getDispatchResp, err := service.GetDispatch(context.Background(), &emergency.GetDispatchRequest{DispatchId: dispatchResp.DispatchId})
		assert.NoError(t, err, "GetDispatch should not return an error")
		assert.Equal(t, updateReq.ArrivedAt, getDispatchResp.Dispatch.ArrivedAt, "ArrivedAt should be updated")

		// Clean up
		defer cleanUpDispatch(t, service, dispatchResp.DispatchId, resource.ResourceId, incident.IncidentId)
	})

	t.Run("DeleteDispatch", func(t *testing.T) {
		// 1. Create incident, resource, and dispatch (similar to DispatchResource test)
		incident, resource := createTestIncidentAndResource(t, service)
		dispatchReq := &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		}
		dispatchResp, err := service.DispatchResource(context.Background(), dispatchReq)
		assert.NoError(t, err, "DispatchResource should not return an error")

		// 2. Delete the dispatch
		deleteResp, err := service.DeleteDispatch(context.Background(), &emergency.DeleteDispatchRequest{DispatchId: dispatchResp.DispatchId})
		assert.NoError(t, err, "DeleteDispatch should not return an error")
		assert.Equal(t, "success", deleteResp.Status, "DeleteDispatch should be successful")

		// 3. Verify that the dispatch is deleted
		_, err = service.GetDispatch(context.Background(), &emergency.GetDispatchRequest{DispatchId: dispatchResp.DispatchId})
		assert.Error(t, err, "GetDispatch should return an error after deletion")
		assert.True(t, status.Code(err) == codes.NotFound, "Error code should be NotFound")

		// Clean up (only incident and resource since dispatch is deleted)
		defer cleanUpResource(t, service, resource.ResourceId)
		defer cleanUpIncident(t, service, incident.IncidentId)
	})

	t.Run("ArriveResource", func(t *testing.T) {
		// 1. Create incident, resource, and dispatch (similar to DispatchResource test)
		incident, resource := createTestIncidentAndResource(t, service)
		dispatchReq := &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		}
		dispatchResp, err := service.DispatchResource(context.Background(), dispatchReq)
		assert.NoError(t, err, "DispatchResource should not return an error")

		// 2. Mark the resource as arrived
		arriveResp, err := service.ArriveResource(context.Background(), &emergency.ArriveResourceRequest{DispatchId: dispatchResp.DispatchId})
		assert.NoError(t, err, "ArriveResource should not return an error")
		assert.Equal(t, "success", arriveResp.Status, "ArriveResource should be successful")

		// 3. Verify that arrived_at is set in the dispatch
		getDispatchResp, err := service.GetDispatch(context.Background(), &emergency.GetDispatchRequest{DispatchId: dispatchResp.DispatchId})
		assert.NoError(t, err, "GetDispatch should not return an error")
		assert.NotEmpty(t, getDispatchResp.Dispatch.ArrivedAt, "ArrivedAt should be set after ArriveResource")

		// Clean up
		defer cleanUpDispatch(t, service, dispatchResp.DispatchId, resource.ResourceId, incident.IncidentId)
	})

	// ------------------ Alert Methods ------------------
	t.Run("CreateAlert", func(t *testing.T) {
		req := &emergency.CreateAlertRequest{
			AlertType:          "Fire",
			Message:            "Test Fire Alert",
			AffectedAreaPoints: []string{"34.0522,-118.2437", "34.0522,-118.2440", "34.0525,-118.2440", "34.0525,-118.2437", "34.0522,-118.2437"},
		}
		resp, err := service.CreateAlert(context.Background(), req)

		assert.NoError(t, err, "CreateAlert should not return an error")
		assert.NotNil(t, resp, "CreateAlert response should not be nil")
		assert.NotEmpty(t, resp.AlertId, "AlertId should be generated")
	})
}

// --- Helper functions for creating and cleaning up test data ---

func createTestIncidentAndResource(t *testing.T, service *EmergencyResponseService) (*emergency.CreateIncidentResponse, *emergency.CreateResourceResponse) {
	incidentReq := &emergency.CreateIncidentRequest{
		IncidentType: "Fire",
		Latitude:     "34.0522",
		Longitude:    "-118.2437",
		Description:  "Test Fire Incident",
	}
	incidentResp, err := service.CreateIncident(context.Background(), incidentReq)
	assert.NoError(t, err, "Failed to create incident for test")

	resourceReq := &emergency.CreateResourceRequest{
		ResourceType: "Fire Truck",
		Latitude:     "34.0500",
		Longitude:    "-118.2400",
		Status:       "available",
	}
	resourceResp, err := service.CreateResource(context.Background(), resourceReq)
	assert.NoError(t, err, "Failed to create resource for test")

	return incidentResp, resourceResp
}

func cleanUpIncident(t *testing.T, service *EmergencyResponseService, incidentID string) {
	_, err := service.Stg.Incident().DeleteIncident(context.Background(), &emergency.DeleteIncidentRequest{IncidentId: incidentID})
	assert.NoError(t, err, fmt.Sprintf("Failed to clean up incident: %s", incidentID))
}

func cleanUpResource(t *testing.T, service *EmergencyResponseService, resourceID string) {
	_, err := service.Stg.Resource().DeleteResource(context.Background(), &emergency.DeleteResourceRequest{ResourceId: resourceID})
	assert.NoError(t, err, fmt.Sprintf("Failed to clean up resource: %s", resourceID))
}

func cleanUpDispatch(t *testing.T, service *EmergencyResponseService, dispatchID, resourceID, incidentID string) {
	_, err := service.Stg.Dispatch().DeleteDispatch(context.Background(), &emergency.DeleteDispatchRequest{DispatchId: dispatchID})
	assert.NoError(t, err, fmt.Sprintf("Failed to clean up dispatch: %s", dispatchID))
	cleanUpResource(t, service, resourceID)
	cleanUpIncident(t, service, incidentID)
}
