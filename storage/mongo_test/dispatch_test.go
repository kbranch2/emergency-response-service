package test

import (
	"context"
	"log"
	"testing"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	mongodb "github.com/smart-city/emergency-response-service/storage/mongo"
	"github.com/stretchr/testify/assert"
)

func TestDispatchMethods(t *testing.T) {
	db := createDBConnection(t)

	incidentRepo := mongodb.NewIncident(db)
	resourceRepo := mongodb.NewResource(db)
	dispatchRepo := mongodb.NewDispatch(db)

	// --- Helper function to create test incident and resource ---
	createTestIncidentAndResource := func(t *testing.T) (*emergency.CreateIncidentResponse, *emergency.CreateResourceResponse) {
		newIncident, err := incidentRepo.CreateIncident(context.Background(), &emergency.CreateIncidentRequest{
			IncidentType: "Fire",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Description:  "Test fire incident",
		})
		assert.NoError(t, err, "Creating incident failed")

		newResource, err := resourceRepo.CreateResource(context.Background(), &emergency.CreateResourceRequest{
			ResourceType: "Fire Truck",
			Latitude:     "34.0500",
			Longitude:    "-118.2400",
			Status:       "available",
		})
		assert.NoError(t, err, "Creating resource failed")

		return newIncident, newResource
	}
	// --- Test DispatchResource ---
	t.Run("TestDispatchResource", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)

		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err, "DispatchResource should not return an error")
		assert.NotNil(t, dispatchRes, "DispatchResource response should not be nil")
		assert.NotEmpty(t, dispatchRes.DispatchId, "Dispatched resource should have a valid ID")

	})

	// --- Test GetDispatch ---
	t.Run("TestGetDispatch", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		getDispatchRes, err := dispatchRepo.GetDispatch(context.Background(), &emergency.GetDispatchRequest{
			DispatchId: dispatchRes.DispatchId,
		})
		assert.NoError(t, err, "GetDispatch should not return an error")
		assert.NotNil(t, getDispatchRes, "GetDispatch response should not be nil")
		log.Println(getDispatchRes)

	})

	// --- Test UpdateDispatch ---
	t.Run("TestUpdateDispatch", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		updateDispatchReq := &emergency.UpdateDispatchRequest{
			DispatchId: dispatchRes.DispatchId,
			IncidentId: incident.IncidentId,
			ResourceId: resource.ResourceId,
			ArrivedAt:  "2024-02-15T10:00:00Z",
		}
		updateDispatchRes, err := dispatchRepo.UpdateDispatch(context.Background(), updateDispatchReq)

		assert.NoError(t, err, "UpdateDispatch should not return an error")
		assert.NotNil(t, updateDispatchRes, "UpdateDispatch response should not be nil")
		assert.Equal(t, "success", updateDispatchRes.Status, "UpdateDispatch should return success status")

	})

	// --- Test DeleteDispatch ---
	t.Run("TestDeleteDispatch", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		deleteDispatchRes, err := dispatchRepo.DeleteDispatch(context.Background(), &emergency.DeleteDispatchRequest{
			DispatchId: dispatchRes.DispatchId,
		})

		assert.NoError(t, err, "DeleteDispatch should not return an error")
		assert.NotNil(t, deleteDispatchRes, "DeleteDispatch response should not be nil")
		assert.Equal(t, "success", deleteDispatchRes.Status, "DeleteDispatch should return success status")

	})

	// --- Test ArriveResource ---
	t.Run("TestArriveResource", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		arriveResourceRes, err := dispatchRepo.ArriveResource(context.Background(), &emergency.ArriveResourceRequest{
			DispatchId: dispatchRes.DispatchId,
		})

		assert.NoError(t, err, "ArriveResource should not return an error")
		assert.NotNil(t, arriveResourceRes, "ArriveResource response should not be nil")
		assert.Equal(t, "success", arriveResourceRes.Status, "ArriveResource should return success status")

	})
}
