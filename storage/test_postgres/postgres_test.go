package test

import (
	"testing"

	"github.com/smart-city/emergency-response-service/storage/postgres"
	"github.com/stretchr/testify/assert"
)

func TestDbConn(t *testing.T) {
	_, err := postgres.DbConn()

	assert.NoError(t, err)
}
