package test

import (
	"context"
	"testing"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"github.com/smart-city/emergency-response-service/storage/postgres"
	"github.com/stretchr/testify/assert"
)

func TestCreateIncident(t *testing.T) {
	db := createDBConnection(t)
	defer db.Close(context.Background())

	incidentRepo := postgres.NewIncident(db)

	testIncident := &emergency.CreateIncidentRequest{
		IncidentType: "Fire",
		Latitude:     "34.0522",
		Longitude:    "-118.2437",
		Description:  "Fire reported in building.",
	}

	response, err := incidentRepo.CreateIncident(context.Background(), testIncident)
	assert.NoError(t, err, "CreateIncident should not return an error")
	assert.NotNil(t, response, "CreateIncident response should not be nil")
	assert.NotEmpty(t, response.IncidentId, "Created incident should have a valid ID")

	_, err = db.Exec(context.Background(), "DELETE FROM emergency_incidents WHERE incident_id = $1", response.IncidentId)
	assert.NoError(t, err, "Cleanup: failed to delete test incident")
}

func TestGetIncident(t *testing.T) {
	db := createDBConnection(t)
	defer db.Close(context.Background())

	incidentRepo := postgres.NewIncident(db)

	// 1. Create an incident to retrieve
	testIncident := &emergency.CreateIncidentRequest{
		IncidentType: "Medical",
		Latitude:     "37.7749",
		Longitude:    "-122.4194",
		Description:  "Person injured.",
	}
	createResponse, err := incidentRepo.CreateIncident(context.Background(), testIncident)
	assert.NoError(t, err, "Creating incident for GetIncident test failed")
	assert.NotEmpty(t, createResponse.IncidentId, "Created incident should have a valid ID")

	// 2. Get the incident
	getReq := &emergency.GetIncidentRequest{IncidentId: createResponse.IncidentId}
	getResponse, err := incidentRepo.GetIncident(context.Background(), getReq)
	assert.NoError(t, err, "GetIncident should not return an error")
	assert.NotNil(t, getResponse, "GetIncident response should not be nil")
	assert.Equal(t, testIncident.IncidentType, getResponse.Incident.IncidentType)
	assert.Equal(t, testIncident.Description, getResponse.Incident.Description)

	// Cleanup
	_, err = db.Exec(context.Background(), "DELETE FROM emergency_incidents WHERE incident_id = $1", createResponse.IncidentId)
	assert.NoError(t, err, "Cleanup: failed to delete test incident")
}

func TestListActiveIncidents(t *testing.T) {
	db := createDBConnection(t)
	defer db.Close(context.Background())

	incidentRepo := postgres.NewIncident(db)

	// 1. Create some incidents, including at least two active incidents
	testIncidents := []*emergency.CreateIncidentRequest{
		{IncidentType: "Fire", Latitude: "34.0522", Longitude: "-118.2437", Description: "Fire incident"},
		{IncidentType: "Traffic", Latitude: "37.7749", Longitude: "-122.4194", Description: "Traffic accident"},
		{IncidentType: "Medical", Latitude: "40.7128", Longitude: "-74.0060", Description: "Medical emergency"},
	}

	createdIncidentIds := make([]string, 0)
	for _, inc := range testIncidents {
		createRes, err := incidentRepo.CreateIncident(context.Background(), inc)
		assert.NoError(t, err, "Creating incident for ListActiveIncidents test failed")
		assert.NotEmpty(t, createRes.IncidentId)
		createdIncidentIds = append(createdIncidentIds, createRes.IncidentId)
	}

	// 2. List active incidents
	listResponse, err := incidentRepo.ListActiveIncidents(context.Background(), &emergency.ListActiveIncidentsRequest{})
	assert.NoError(t, err, "ListActiveIncidents should not return an error")
	assert.NotNil(t, listResponse, "ListActiveIncidents response should not be nil")
	assert.GreaterOrEqual(t, len(listResponse.Incidents), 2, "Should have at least two active incidents")

	// Cleanup
	for _, id := range createdIncidentIds {
		_, err = db.Exec(context.Background(), "DELETE FROM emergency_incidents WHERE incident_id = $1", id)
		assert.NoError(t, err, "Cleanup: failed to delete test incident")
	}
}

func TestUpdateIncidentStatus(t *testing.T) {
	db := createDBConnection(t)
	defer db.Close(context.Background())

	incidentRepo := postgres.NewIncident(db)

	// 1. Create an incident
	testIncident := &emergency.CreateIncidentRequest{
		IncidentType: "Fire",
		Latitude:     "34.0522",
		Longitude:    "-118.2437",
		Description:  "Fire incident",
	}
	createResponse, err := incidentRepo.CreateIncident(context.Background(), testIncident)
	assert.NoError(t, err, "Creating incident for UpdateIncidentStatus test failed")
	assert.NotEmpty(t, createResponse.IncidentId)

	// 2. Update the incident status
	updateReq := &emergency.UpdateIncidentStatusRequest{
		IncidentId: createResponse.IncidentId,
		Status:     "resolved", // New status
	}
	updateResponse, err := incidentRepo.UpdateIncidentStatus(context.Background(), updateReq)
	assert.NoError(t, err, "UpdateIncidentStatus should not return an error")
	assert.NotNil(t, updateResponse, "UpdateIncidentStatus response should not be nil")
	assert.Equal(t, "success", updateResponse.Status, "Update should be successful")

	// 3. Retrieve the incident and verify the status
	getReq := &emergency.GetIncidentRequest{IncidentId: createResponse.IncidentId}
	getRes, err := incidentRepo.GetIncident(context.Background(), getReq)
	assert.NoError(t, err, "GetIncident after status update should not return an error")
	assert.Equal(t, updateReq.Status, getRes.Incident.Status, "Incident status should be updated")

	// Cleanup
	_, err = db.Exec(context.Background(), "DELETE FROM emergency_incidents WHERE incident_id = $1", createResponse.IncidentId)
	assert.NoError(t, err, "Cleanup: failed to delete test incident")
}

func TestDeleteIncident(t *testing.T) {
	db := createDBConnection(t)
	defer db.Close(context.Background())

	incidentRepo := postgres.NewIncident(db)

	// 1. Create an incident
	testIncident := &emergency.CreateIncidentRequest{
		IncidentType: "Fire",
		Latitude:     "34.0522",
		Longitude:    "-118.2437",
		Description:  "Fire incident",
	}
	createRes, err := incidentRepo.CreateIncident(context.Background(), testIncident)
	assert.NoError(t, err, "Creating incident for DeleteIncident test failed")
	assert.NotEmpty(t, createRes.IncidentId)

	// 2. Delete the incident
	deleteReq := &emergency.DeleteIncidentRequest{IncidentId: createRes.IncidentId}
	deleteRes, err := incidentRepo.DeleteIncident(context.Background(), deleteReq)
	assert.NoError(t, err, "DeleteIncident should not return an error")
	assert.NotNil(t, deleteRes, "DeleteIncident response should not be nil")
	assert.Equal(t, "success", deleteRes.Status, "Delete should be successful")

	// 3. Attempt to retrieve the deleted incident (should fail)
	getReq := &emergency.GetIncidentRequest{IncidentId: createRes.IncidentId}
	getRes, err := incidentRepo.GetIncident(context.Background(), getReq)
	assert.Error(t, err, "GetIncident after delete should return an error")
	assert.Nil(t, getRes, "GetIncident response should be nil after delete")
}
