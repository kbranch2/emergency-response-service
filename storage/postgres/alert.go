package postgres

import (
	"context"
	"fmt"
	"strings"

	"github.com/jackc/pgx/v5"
	"github.com/smart-city/emergency-response-service/genproto/emergency"
)

type AlertRepo struct {
	Db *pgx.Conn
}

func NewAlert(db *pgx.Conn) *AlertRepo {
	return &AlertRepo{Db: db}
}
func (alertRepo *AlertRepo) CreateAlert(ctx context.Context, alert *emergency.CreateAlertRequest) (*emergency.CreateAlertResponse, error) {
	// Construct the Polygon string from the affected_area_points
	polygonStr := "POLYGON(("
	for i, point := range alert.AffectedAreaPoints {
		if i > 0 {
			polygonStr += ","
		}
		// Assuming points are in "lat,long" format
		point = strings.ReplaceAll(point, ",", "  ")
		polygonStr += point
	}
	polygonStr += "))"

	var alertID string
	err := alertRepo.Db.QueryRow(ctx,
		`INSERT INTO
			emergency_alerts(
				alert_type, 
				message, 
				affected_area, 
				issued_at
			) 
		 VALUES (
		 	$1, 
			$2, 
			ST_GeomFromText($3, 4326), 
			NOW()) 
		RETURNING
			alert_id;`,
		alert.AlertType, alert.Message, polygonStr).Scan(&alertID)

	if err != nil {
		return nil, fmt.Errorf("error creating alert: %w", err)
	}

	return &emergency.CreateAlertResponse{
		AlertId: alertID,
	}, nil
}
