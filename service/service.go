package service

import (
	"context"
	"log"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"github.com/smart-city/emergency-response-service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// EmergencyResponseService implements the emergency.EmergencyResponseServiceServer interface.
type EmergencyResponseService struct {
	Stg storage.StorageI
	emergency.UnimplementedEmergencyResponseServiceServer
}

// NewEmergencyResponseService creates a new EmergencyResponseService.
func NewEmergencyResponseService(Stg storage.StorageI) *EmergencyResponseService {
	return &EmergencyResponseService{Stg: Stg}
}

// CreateIncident creates a new incident.
func (s *EmergencyResponseService) CreateIncident(ctx context.Context, req *emergency.CreateIncidentRequest) (*emergency.CreateIncidentResponse, error) {
	// Basic input validation (you can add more as needed)
	if req.IncidentType == "" || req.Latitude == "" || req.Longitude == "" || req.Description == "" {
		return nil, status.Error(codes.InvalidArgument, "missing required fields")
	}

	resp, err := s.Stg.Incident().CreateIncident(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error creating incident: %v", err)
	}
	return resp, nil
}

// GetIncident gets an incident by its ID.
func (s *EmergencyResponseService) GetIncident(ctx context.Context, req *emergency.GetIncidentRequest) (*emergency.GetIncidentResponse, error) {
	if req.IncidentId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing incident ID")
	}

	resp, err := s.Stg.Incident().GetIncident(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "incident not found: %v", err)
	}
	return resp, nil
}

// ListActiveIncidents lists all active incidents.
func (s *EmergencyResponseService) ListActiveIncidents(ctx context.Context, req *emergency.ListActiveIncidentsRequest) (*emergency.ListActiveIncidentsResponse, error) {
	resp, err := s.Stg.Incident().ListActiveIncidents(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error listing active incidents: %v", err)
	}
	return resp, nil
}

// UpdateIncidentStatus updates the status of an incident.
func (s *EmergencyResponseService) UpdateIncidentStatus(ctx context.Context, req *emergency.UpdateIncidentStatusRequest) (*emergency.UpdateIncidentStatusResponse, error) {
	if req.IncidentId == "" || req.Status == "" {
		return nil, status.Error(codes.InvalidArgument, "missing incident ID or status")
	}

	resp, err := s.Stg.Incident().UpdateIncidentStatus(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error updating incident status: %v", err)
	}
	return resp, nil
}

// DeleteIncident deletes an incident by its ID.
func (s *EmergencyResponseService) DeleteIncident(ctx context.Context, req *emergency.DeleteIncidentRequest) (*emergency.DeleteIncidentResponse, error) {
	if req.IncidentId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing incident ID")
	}

	resp, err := s.Stg.Incident().DeleteIncident(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error deleting incident: %v", err)
	}
	return resp, nil
}

// ------------------ Resource Methods ------------------

// CreateResource creates a new resource.
func (s *EmergencyResponseService) CreateResource(ctx context.Context, req *emergency.CreateResourceRequest) (*emergency.CreateResourceResponse, error) {
	if req.ResourceType == "" || req.Latitude == "" || req.Longitude == "" {
		return nil, status.Error(codes.InvalidArgument, "missing required fields for resource creation")
	}

	resp, err := s.Stg.Resource().CreateResource(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error creating resource: %v", err)
	}
	return resp, nil
}

// GetResource gets a resource by its ID.
func (s *EmergencyResponseService) GetResource(ctx context.Context, req *emergency.GetResourceRequest) (*emergency.GetResourceResponse, error) {
	if req.ResourceId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing resource ID")
	}

	resp, err := s.Stg.Resource().GetResource(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "resource not found: %v", err)
	}
	return resp, nil
}

// UpdateResource updates a resource.
func (s *EmergencyResponseService) UpdateResource(ctx context.Context, req *emergency.UpdateResourceRequest) (*emergency.UpdateResourceResponse, error) {
	if req.ResourceId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing resource ID")
	}

	resp, err := s.Stg.Resource().UpdateResource(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error updating resource: %v", err)
	}
	return resp, nil
}

// DeleteResource deletes a resource by its ID.
func (s *EmergencyResponseService) DeleteResource(ctx context.Context, req *emergency.DeleteResourceRequest) (*emergency.DeleteResourceResponse, error) {
	if req.ResourceId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing resource ID")
	}

	resp, err := s.Stg.Resource().DeleteResource(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error deleting resource: %v", err)
	}
	return resp, nil
}

// UpdateResourceStatus updates the status of a resource.
func (s *EmergencyResponseService) UpdateResourceStatus(ctx context.Context, req *emergency.UpdateResourceStatusRequest) (*emergency.UpdateResourceStatusResponse, error) {
	if req.ResourceId == "" || req.Status == "" {
		return nil, status.Error(codes.InvalidArgument, "missing resource ID or status")
	}

	resp, err := s.Stg.Resource().UpdateResourceStatus(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error updating resource status: %v", err)
	}
	return resp, nil
}

// ListAvailableResources lists available resources based on emergency type.
func (s *EmergencyResponseService) ListAvailableResources(ctx context.Context, req *emergency.ListAvailableResourcesRequest) (*emergency.ListAvailableResourcesResponse, error) {
	if req.EmergencyType == "" {
		return nil, status.Error(codes.InvalidArgument, "missing emergency type")
	}

	resp, err := s.Stg.Resource().ListAvailableResources(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error listing available resources: %v", err)
	}
	return resp, nil
}

// ------------------ Dispatch Methods ------------------

// DispatchResource dispatches a resource to an incident.
func (s *EmergencyResponseService) DispatchResource(ctx context.Context, req *emergency.DispatchResourceRequest) (*emergency.DispatchResourceResponse, error) {
	if req.ResourceId == "" || req.IncidentId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing resource ID or incident ID")
	}

	resp, err := s.Stg.Dispatch().DispatchResource(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error dispatching resource: %v", err)
	}
	return resp, nil
}

// GetDispatch gets a dispatch by its ID.
func (s *EmergencyResponseService) GetDispatch(ctx context.Context, req *emergency.GetDispatchRequest) (*emergency.GetDispatchResponse, error) {
	if req.DispatchId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing dispatch ID")
	}

	resp, err := s.Stg.Dispatch().GetDispatch(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.NotFound, "dispatch not found: %v", err)
	}
	return resp, nil
}

// UpdateDispatch updates a dispatch.
func (s *EmergencyResponseService) UpdateDispatch(ctx context.Context, req *emergency.UpdateDispatchRequest) (*emergency.UpdateDispatchResponse, error) {
	if req.DispatchId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing dispatch ID")
	}

	resp, err := s.Stg.Dispatch().UpdateDispatch(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error updating dispatch: %v", err)
	}
	return resp, nil
}

// DeleteDispatch deletes a dispatch by its ID.
func (s *EmergencyResponseService) DeleteDispatch(ctx context.Context, req *emergency.DeleteDispatchRequest) (*emergency.DeleteDispatchResponse, error) {
	if req.DispatchId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing dispatch ID")
	}

	resp, err := s.Stg.Dispatch().DeleteDispatch(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error deleting dispatch: %v", err)
	}
	return resp, nil
}

// ArriveResource marks a resource as arrived at the incident location.
func (s *EmergencyResponseService) ArriveResource(ctx context.Context, req *emergency.ArriveResourceRequest) (*emergency.ArriveResourceResponse, error) {
	if req.DispatchId == "" {
		return nil, status.Error(codes.InvalidArgument, "missing dispatch ID")
	}

	resp, err := s.Stg.Dispatch().ArriveResource(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error updating arrival status: %v", err)
	}
	return resp, nil
}

// ------------------ Alert Methods ------------------

// CreateAlert creates a new emergency alert.
func (s *EmergencyResponseService) CreateAlert(ctx context.Context, req *emergency.CreateAlertRequest) (*emergency.CreateAlertResponse, error) {
	if req.AlertType == "" || req.Message == "" || len(req.AffectedAreaPoints) == 0 {
		return nil, status.Error(codes.InvalidArgument, "missing required fields for alert creation")
	}
	resp, err := s.Stg.Alert().CreateAlert(ctx, req)
	if err != nil {
		return nil, status.Errorf(codes.Internal, "error creating alert: %v", err)
	}
	log.Println("Hello")
	return resp, nil
}
