CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE emergency_incidents (
    incident_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    incident_type VARCHAR(50) NOT NULL,
    location GEOMETRY(POINT, 4326) NOT NULL,
    description TEXT NOT NULL,
    status VARCHAR(20) NOT NULL, -- 'active', 'resolved', 'false_alarm'
    reported_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE emergency_resources (
    resource_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    resource_type VARCHAR(50) NOT NULL,
    current_location GEOMETRY(POINT, 4326) NOT NULL,
    status VARCHAR(20) NOT NULL -- 'available', 'dispatched', 'unavailable'
);

CREATE TABLE resource_dispatches (
    dispatch_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    incident_id UUID REFERENCES emergency_incidents(incident_id), 
    resource_id UUID REFERENCES emergency_resources(resource_id),
    dispatched_at TIMESTAMP WITH TIME ZONE NOT NULL,
    arrived_at TIMESTAMP WITH TIME ZONE 
);

CREATE TABLE emergency_alerts (
    alert_id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    alert_type VARCHAR(50) NOT NULL,
    message TEXT NOT NULL,
    affected_area GEOMETRY(Polygon,4326) NOT NULL,
    issued_at TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
