# Emergency Response Service

This project implements an Emergency Response Service using gRPC and a choice of either PostgreSQL (with PostGIS extension) or MongoDB as the database.

## Project Structure:

- **`genproto/emergency`:** Contains the Protobuf definition (`emergency.proto`) for the gRPC service.
- **`storage`:** Defines the storage interface and concrete implementations for PostgreSQL and MongoDB.
- **`storage/postgres`:** PostgreSQL repository implementations.
- **`storage/mongodb`:** MongoDB repository implementations.
- **`service`:** Implements the gRPC service logic.
- **`test`:** Contains tests for the service and repository methods.
- **`config`:** Handles configuration loading (from files or environment variables).
- **`main.go`:** The main application file that starts the gRPC server.

## Features:

- **Incident Management:**

  - Create incidents with details like type, location, and description.
  - Get incident details by ID.
  - List active incidents.
  - Update incident status (e.g., active, resolved, false alarm).
  - Delete incidents.

- **Resource Management:**

  - Create resources (e.g., ambulances, fire trucks, police cars) with type, location, and availability status.
  - Get resource details by ID.
  - Update resource details.
  - Delete resources.
  - Update resource status (e.g., available, dispatched, unavailable).
  - List available resources by emergency type.

- **Dispatch Management:**

  - Dispatch a resource to an incident, recording dispatch time.
  - Get dispatch details by ID.
  - Update dispatch details (e.g., arrival time).
  - Delete dispatches.
  - Mark a resource as arrived at an incident, updating the arrival time.

- **Alert Management:**
  - Create emergency alerts with type, message, and affected area (defined as a polygon using geographic coordinates).

## Technologies Used:

- **gRPC:** For defining and implementing the service API and communication.
- **Protocol Buffers:** For defining the data structures (messages) used in gRPC communication.
- **PostgreSQL (with PostGIS):** A powerful relational database with geospatial support for storing incidents, resources, and alerts with location data.
- **MongoDB:** A NoSQL document database that can also be used for storing the application data.
- **Go:** The programming language used for implementing the service.

## Getting Started:

1. **Prerequisites:**

   - Go (1.13 or later) installed on your system.
   - Docker and Docker Compose (recommended for easy setup of PostgreSQL and MongoDB).
   - Protobuf compiler (`protoc`).

2. **Database Setup:**

   - **PostgreSQL:**
     - Use Docker Compose to start a PostgreSQL instance with PostGIS extension enabled:
       ```yaml
       version: "3.7"
       services:
         postgres:
           image: postgis/postgis:13-3.1
           environment:
             POSTGRES_USER: your_db_user
             POSTGRES_PASSWORD: your_db_password
             POSTGRES_DB: your_db_name
           ports:
             - "5432:5432"
       ```
       - Replace placeholders with your desired values.
       - Run `docker-compose up -d` to start the database container.
   - **MongoDB:**
     - Use Docker Compose to start a MongoDB instance:
       ```yaml
       version: "3.7"
       services:
         mongo:
           image: mongo:latest
           ports:
             - "27017:27017"
       ```
       - Run `docker-compose up -d` to start the database container.

3. **Generate gRPC Code:**

   - Navigate to the `genproto/emergency` directory.
   - Run `protoc --go_out=../ --go_opt=paths=source_relative --go-grpc_out=../ --go-grpc_opt=paths=source_relative emergency.proto` to generate the Go code for the gRPC service definition.

4. **Configuration:**

   - Create a configuration file (e.g., `config.yaml`) or set environment variables with the following information:
     - Database connection details (host, port, user, password, database name).
     - gRPC server port (e.g., `8088`).

5. **Build and Run:**
   - Navigate to the project root directory.
   - Build the application: `go build`
   - Run the application: `./emergency-response-service`

## Testing:

- The project includes unit tests for both the service and repository layers.
- You can run the tests using `go test -v ./...`.

## API Documentation:

- The gRPC service API is defined in the `emergency.proto` file. You can use tools like `grpcurl` or generate documentation using `protoc-gen-doc` to explore the API.

## Future Enhancements:

- **Real-time Updates:** Implement real-time updates to clients (e.g., using WebSockets or Server-Sent Events) for changes in incident and resource status.
- **User Authentication and Authorization:** Add authentication and authorization mechanisms to secure the service.
- **Integration with External Systems:** Integrate with external systems like mapping services, SMS gateways, or other emergency response platforms.
- **Advanced Analytics and Reporting:** Develop dashboards and reports to analyze emergency response data and identify patterns or areas for improvement.

## Contributing:

- Contributions to this project are welcome! Feel free to open issues or submit pull requests.
