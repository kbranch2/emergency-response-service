package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	"github.com/jackc/pgx/v5"
	"github.com/smart-city/emergency-response-service/genproto/emergency"
)

type DispatchRepo struct {
	Db *pgx.Conn
}

func NewDispatch(db *pgx.Conn) *DispatchRepo {
	return &DispatchRepo{Db: db}
}

func (d *DispatchRepo) DispatchResource(ctx context.Context, req *emergency.DispatchResourceRequest) (*emergency.DispatchResourceResponse, error) {
	var dispatchID string

	tx, err := d.Db.Begin(ctx)
	if err != nil {
		return nil, fmt.Errorf("error starting transaction: %w", err)
	}
	defer tx.Rollback(ctx) // Rollback if any query fails

	// 1. Create a new dispatch record
	err = tx.QueryRow(ctx, `
		INSERT INTO 
			resource_dispatches (
				incident_id, 
				resource_id, 
				dispatched_at
				)
		VALUES (
			$1, 
			$2, 
			NOW())
		RETURNING dispatch_id;
	`, req.IncidentId, req.ResourceId).Scan(&dispatchID)
	if err != nil {
		return nil, fmt.Errorf("error creating dispatch record: %w", err)
	}

	// 2. Update resource status to "dispatched"
	_, err = tx.Exec(ctx, `
		UPDATE emergency_resources
		SET status = 'dispatched'
		WHERE resource_id = $1;
	`, req.ResourceId)
	if err != nil {
		return nil, fmt.Errorf("error updating resource status: %w", err)
	}

	// 3. Commit transaction if both operations were successful
	if err := tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("error committing transaction: %w", err)
	}

	return &emergency.DispatchResourceResponse{DispatchId: dispatchID}, nil
}
func (d *DispatchRepo) GetDispatch(ctx context.Context, req *emergency.GetDispatchRequest) (*emergency.GetDispatchResponse, error) {
	var (
		dispatchedAt sql.NullTime
		arrivedAt    sql.NullTime
	)
	dispatch := &emergency.ResourceDispatch{}
	err := d.Db.QueryRow(ctx, `
        SELECT dispatch_id, 
            incident_id, 
            resource_id, 
            dispatched_at, 
            arrived_at
        FROM 
            resource_dispatches
        WHERE 
            dispatch_id = $1;
    `, req.DispatchId).Scan(
		&dispatch.DispatchId, &dispatch.IncidentId, &dispatch.ResourceId,
		&dispatchedAt, &arrivedAt,
	)
	if err != nil {
		return nil, fmt.Errorf("error getting dispatch: %w", err)
	}

	// Handle the potential NULL values
	if dispatchedAt.Valid {
		dispatch.DispatchedAt = dispatchedAt.Time.Format(time.RFC3339)
	} else {
		dispatch.DispatchedAt = ""
	}

	if arrivedAt.Valid {
		dispatch.ArrivedAt = arrivedAt.Time.Format(time.RFC3339)
	} else {
		dispatch.ArrivedAt = ""
	}

	return &emergency.GetDispatchResponse{Dispatch: dispatch}, nil
}

func (d *DispatchRepo) UpdateDispatch(ctx context.Context, req *emergency.UpdateDispatchRequest) (*emergency.UpdateDispatchResponse, error) {
	arrived_at, err := time.Parse(time.RFC3339, req.ArrivedAt)
	if err != nil {
		return nil, err
	}
	commandTag, err := d.Db.Exec(ctx, `
		UPDATE 
			resource_dispatches
		SET 
			incident_id = $1, 
			resource_id = $2, 
			arrived_at = $3
		WHERE 
			dispatch_id = $4;
	`, req.IncidentId, req.ResourceId, arrived_at, req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("error updating dispatch: %w", err)
	}

	if commandTag.RowsAffected() == 0 {
		return &emergency.UpdateDispatchResponse{Status: "error: dispatch not found"}, nil
	}
	return &emergency.UpdateDispatchResponse{Status: "success"}, nil
}

func (d *DispatchRepo) DeleteDispatch(ctx context.Context, req *emergency.DeleteDispatchRequest) (*emergency.DeleteDispatchResponse, error) {
	commandTag, err := d.Db.Exec(ctx, `
		DELETE FROM resource_dispatches
		WHERE dispatch_id = $1;
	`, req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("error deleting dispatch: %w", err)
	}

	if commandTag.RowsAffected() == 0 {
		return &emergency.DeleteDispatchResponse{Status: "error: dispatch not found"}, nil
	}
	return &emergency.DeleteDispatchResponse{Status: "success"}, nil
}

func (d *DispatchRepo) ArriveResource(ctx context.Context, req *emergency.ArriveResourceRequest) (*emergency.ArriveResourceResponse, error) {
	tx, err := d.Db.Begin(ctx)
	if err != nil {
		return nil, fmt.Errorf("error starting transaction: %w", err)
	}
	defer tx.Rollback(ctx)

	// 1. Update arrived_at in resource_dispatches
	_, err = tx.Exec(ctx, `
        UPDATE resource_dispatches
        SET arrived_at = NOW()
        WHERE dispatch_id = $1;
    `, req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("error updating dispatch with arrival time: %w", err)
	}

	// 2. Update resource status to 'available' (assuming the resource is available after arrival)
	_, err = tx.Exec(ctx, `
        UPDATE 
			emergency_resources
        SET 
			status = 'available'
        WHERE 
			resource_id = (
							SELECT 
								resource_id 
							FROM 
								resource_dispatches 
							WHERE 
								dispatch_id = $1);
    `, req.DispatchId)
	if err != nil {
		return nil, fmt.Errorf("error updating resource status to available: %w", err)
	}

	if err := tx.Commit(ctx); err != nil {
		return nil, fmt.Errorf("error committing transaction: %w", err)
	}

	return &emergency.ArriveResourceResponse{Status: "success"}, nil
}
