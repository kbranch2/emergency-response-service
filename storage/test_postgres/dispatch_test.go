package test

import (
	"context"
	"testing"

	"github.com/jackc/pgx/v5"
	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"github.com/smart-city/emergency-response-service/storage/postgres"
	"github.com/stretchr/testify/assert"
)

func TestDispatchMethods(t *testing.T) {
	db := createDBConnection(t)
	defer db.Close(context.Background())

	incidentRepo := postgres.NewIncident(db)
	resourceRepo := postgres.NewResource(db)
	dispatchRepo := postgres.NewDispatch(db)

	// --- Helper function to create test incident and resource ---
	createTestIncidentAndResource := func(t *testing.T) (*emergency.CreateIncidentResponse, *emergency.CreateResourceResponse) {
		newIncident, err := incidentRepo.CreateIncident(context.Background(), &emergency.CreateIncidentRequest{
			IncidentType: "Fire",
			Latitude:     "34.0522",
			Longitude:    "-118.2437",
			Description:  "Test fire incident",
		})
		assert.NoError(t, err, "Creating incident failed")

		newResource, err := resourceRepo.CreateResource(context.Background(), &emergency.CreateResourceRequest{
			ResourceType: "Fire Truck",
			Latitude:     "34.0500",
			Longitude:    "-118.2400",
			Status:       "available",
		})
		assert.NoError(t, err, "Creating resource failed")

		return newIncident, newResource
	}

	// --- Test DispatchResource ---
	t.Run("TestDispatchResource", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)

		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err, "DispatchResource should not return an error")
		assert.NotNil(t, dispatchRes, "DispatchResource response should not be nil")
		assert.NotEmpty(t, dispatchRes.DispatchId, "Dispatched resource should have a valid ID")

		// Cleanup
		defer cleanUpDispatch(t, db, dispatchRes.DispatchId, resource.ResourceId, incident.IncidentId)
	})

	// --- Test GetDispatch ---
	t.Run("TestGetDispatch", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		getDispatchRes, err := dispatchRepo.GetDispatch(context.Background(), &emergency.GetDispatchRequest{
			DispatchId: dispatchRes.DispatchId,
		})
		assert.NoError(t, err, "GetDispatch should not return an error")
		assert.NotNil(t, getDispatchRes, "GetDispatch response should not be nil")
		assert.Equal(t, dispatchRes.DispatchId, getDispatchRes.Dispatch.DispatchId, "Dispatch IDs should match")

		// Cleanup
		defer cleanUpDispatch(t, db, dispatchRes.DispatchId, resource.ResourceId, incident.IncidentId)
	})

	// --- Test UpdateDispatch ---
	t.Run("TestUpdateDispatch", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		updateDispatchReq := &emergency.UpdateDispatchRequest{
			DispatchId: dispatchRes.DispatchId,
			IncidentId: incident.IncidentId,
			ResourceId: resource.ResourceId,
			ArrivedAt:  "2024-02-15T10:00:00Z",
		}
		updateDispatchRes, err := dispatchRepo.UpdateDispatch(context.Background(), updateDispatchReq)

		assert.NoError(t, err, "UpdateDispatch should not return an error")
		assert.NotNil(t, updateDispatchRes, "UpdateDispatch response should not be nil")
		assert.Equal(t, "success", updateDispatchRes.Status, "UpdateDispatch should return success status")

		// Cleanup
		defer cleanUpDispatch(t, db, dispatchRes.DispatchId, resource.ResourceId, incident.IncidentId)
	})

	// --- Test DeleteDispatch ---
	t.Run("TestDeleteDispatch", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		deleteDispatchRes, err := dispatchRepo.DeleteDispatch(context.Background(), &emergency.DeleteDispatchRequest{
			DispatchId: dispatchRes.DispatchId,
		})

		assert.NoError(t, err, "DeleteDispatch should not return an error")
		assert.NotNil(t, deleteDispatchRes, "DeleteDispatch response should not be nil")
		assert.Equal(t, "success", deleteDispatchRes.Status, "DeleteDispatch should return success status")

		// Cleanup (incident and resource) - Dispatch should be already deleted
		defer cleanUpDispatch(t, db, "", resource.ResourceId, incident.IncidentId)
	})

	// --- Test ArriveResource ---
	t.Run("TestArriveResource", func(t *testing.T) {
		incident, resource := createTestIncidentAndResource(t)
		dispatchRes, err := dispatchRepo.DispatchResource(context.Background(), &emergency.DispatchResourceRequest{
			ResourceId: resource.ResourceId,
			IncidentId: incident.IncidentId,
		})
		assert.NoError(t, err)

		arriveResourceRes, err := dispatchRepo.ArriveResource(context.Background(), &emergency.ArriveResourceRequest{
			DispatchId: dispatchRes.DispatchId,
		})

		assert.NoError(t, err, "ArriveResource should not return an error")
		assert.NotNil(t, arriveResourceRes, "ArriveResource response should not be nil")
		assert.Equal(t, "success", arriveResourceRes.Status, "ArriveResource should return success status")

		// Cleanup
		defer cleanUpDispatch(t, db, dispatchRes.DispatchId, resource.ResourceId, incident.IncidentId)
	})
}

// Helper function to clean up after dispatch tests
func cleanUpDispatch(t *testing.T, db *pgx.Conn, dispatchID, resourceID, incidentID string) {
	if dispatchID != "" {
		_, err := db.Exec(context.Background(), "DELETE FROM resource_dispatches WHERE dispatch_id = $1", dispatchID)
		assert.NoError(t, err, "Cleanup: failed to delete test dispatch")
	}
	if resourceID != "" {
		_, err := db.Exec(context.Background(), "DELETE FROM emergency_resources WHERE resource_id = $1", resourceID)
		assert.NoError(t, err, "Cleanup: failed to delete test resource")
	}
	if incidentID != "" {
		_, err := db.Exec(context.Background(), "DELETE FROM emergency_incidents WHERE incident_id = $1", incidentID)
		assert.NoError(t, err, "Cleanup: failed to delete test incident")
	}
}
