package storage

import (
	"context"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
)

type StorageI interface {
	Incident() IncidentI
	Resource() ResourceI
	Dispatch() DispatchI
	Alert() AlertI
}

type IncidentI interface {
	CreateIncident(ctx context.Context, in *emergency.CreateIncidentRequest) (*emergency.CreateIncidentResponse, error)
	GetIncident(ctx context.Context, in *emergency.GetIncidentRequest) (*emergency.GetIncidentResponse, error)
	ListActiveIncidents(ctx context.Context, in *emergency.ListActiveIncidentsRequest) (*emergency.ListActiveIncidentsResponse, error)
	UpdateIncidentStatus(ctx context.Context, in *emergency.UpdateIncidentStatusRequest) (*emergency.UpdateIncidentStatusResponse, error)
	DeleteIncident(ctx context.Context, in *emergency.DeleteIncidentRequest) (*emergency.DeleteIncidentResponse, error)
}

type ResourceI interface {
	CreateResource(ctx context.Context, in *emergency.CreateResourceRequest) (*emergency.CreateResourceResponse, error)
	GetResource(ctx context.Context, in *emergency.GetResourceRequest) (*emergency.GetResourceResponse, error)
	UpdateResource(ctx context.Context, in *emergency.UpdateResourceRequest) (*emergency.UpdateResourceResponse, error)
	DeleteResource(ctx context.Context, in *emergency.DeleteResourceRequest) (*emergency.DeleteResourceResponse, error)
	UpdateResourceStatus(ctx context.Context, in *emergency.UpdateResourceStatusRequest) (*emergency.UpdateResourceStatusResponse, error)
	ListAvailableResources(ctx context.Context, in *emergency.ListAvailableResourcesRequest) (*emergency.ListAvailableResourcesResponse, error)
}
type DispatchI interface {
	DispatchResource(ctx context.Context, in *emergency.DispatchResourceRequest) (*emergency.DispatchResourceResponse, error)
	GetDispatch(ctx context.Context, in *emergency.GetDispatchRequest) (*emergency.GetDispatchResponse, error)
	UpdateDispatch(ctx context.Context, in *emergency.UpdateDispatchRequest) (*emergency.UpdateDispatchResponse, error)
	DeleteDispatch(ctx context.Context, in *emergency.DeleteDispatchRequest) (*emergency.DeleteDispatchResponse, error)
	ArriveResource(ctx context.Context, in *emergency.ArriveResourceRequest) (*emergency.ArriveResourceResponse, error)
}

type AlertI interface {
	CreateAlert(ctx context.Context, in *emergency.CreateAlertRequest) (*emergency.CreateAlertResponse, error)
}
