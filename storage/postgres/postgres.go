package postgres

import (
	"context"
	"fmt"
	"log/slog"

	"github.com/jackc/pgx/v5"
	"github.com/smart-city/emergency-response-service/config"
	"github.com/smart-city/emergency-response-service/storage"
)

type Storage struct {
	Db         pgx.Conn
	IncidentS  storage.IncidentI
	ResuourceS storage.ResourceI
	AlertS     storage.AlertI
	DispatchS  storage.DispatchI
}

func DbConn() (*Storage, error) {
	var (
		err error
	)
	// Get postgres connection data from .env file
	cfg := config.Load()
	dbCon := fmt.Sprintf("postgresql://%s:%s@%s:%d/%s",
		cfg.DB_USER,
		cfg.DB_PASSWORD,
		cfg.DB_HOST,
		uint16(cfg.DB_PORT),
		cfg.DB_NAME,
	)
	// Connecting to postgres
	db, err := pgx.Connect(context.Background(), dbCon)
	if err != nil {
		slog.Warn("Unable to connect to database:", err)
		return nil, err
	}
	err = db.Ping(context.Background())
	if err != nil {
		slog.Warn("Unable to connect to database:", err)
		return nil, err
	}
	incident := NewIncident(db)
	recourse := NewResource(db)
	alert := NewAlert(db)
	dispatch := NewDispatch(db)
	return &Storage{
		IncidentS:  incident,
		ResuourceS: recourse,
		AlertS:     alert,
		DispatchS:  dispatch,
	}, nil
}

func (st *Storage) Incident() storage.IncidentI {
	if st.IncidentS == nil {
		st.IncidentS = NewIncident(&st.Db)
	}
	return st.IncidentS
}
func (st *Storage) Resource() storage.ResourceI {
	if st.ResuourceS == nil {
		st.ResuourceS = NewResource(&st.Db)
	}
	return st.ResuourceS
}
func (st *Storage) Dispatch() storage.DispatchI {
	if st.DispatchS == nil {
		st.DispatchS = NewDispatch(&st.Db)
	}
	return st.DispatchS
}
func (st *Storage) Alert() storage.AlertI {
	if st.AlertS == nil {
		st.AlertS = NewAlert(&st.Db)
	}
	return st.AlertS
}
