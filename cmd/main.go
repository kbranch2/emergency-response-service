package main

import (
	"log"
	"net"

	"github.com/smart-city/emergency-response-service/genproto/emergency"
	"github.com/smart-city/emergency-response-service/service"
	"github.com/smart-city/emergency-response-service/storage/postgres"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {

	// Connect to the database
	strg, err := postgres.DbConn()
	if err != nil {
		log.Fatal(err)
	}

	// Create storage and service instances

	srv := service.NewEmergencyResponseService(strg)

	// Start gRPC server
	lis, err := net.Listen("tcp", ":8088")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	emergency.RegisterEmergencyResponseServiceServer(s, srv)

	// Register reflection service on gRPC server (for debugging/testing)
	reflection.Register(s)

	log.Printf("gRPC server listening on :8088")
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
