package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

type Config struct {
	HTTPPort string

	DB_HOST     string
	DB_PORT     int
	DB_USER     string
	DB_PASSWORD string
	DB_NAME     string

	DefaultOffset string
	DefaultLimit  string
}

// Load ...
func Load() Config {
	if err := godotenv.Load(".env"); err != nil {

		fmt.Println("No .env file found", err)
	}

	config := Config{}

	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8080"))

	config.DB_HOST = cast.ToString(getOrReturnDefaultValue("DB_HOST", "localhost"))
	config.DB_PORT = cast.ToInt(getOrReturnDefaultValue("DB_PORT", 5432))
	config.DB_USER = cast.ToString(getOrReturnDefaultValue("DB_USER", "sayyidmuhammad"))
	config.DB_PASSWORD = cast.ToString(getOrReturnDefaultValue("DB_PASSWORD", "root"))
	config.DB_NAME = cast.ToString(getOrReturnDefaultValue("DB_NAME", "emergency"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val := os.Getenv(key)

	if val != "" {
		return val
	}

	return defaultValue
}
